﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelButton : MonoBehaviour
{
    [SerializeField]
    private GameObject m_pausePanel;

    public void ResetScene(int number)
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(number); 
    }
}
