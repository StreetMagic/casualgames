﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BoxManager : MonoBehaviour
{
    public static BoxManager Instance { get; private set; }

    [SerializeField]
    public int m_SectorCount;

    [SerializeField]
    private GameObject m_Sector;

    private BoxCollider2D box;
    private float sectorHight;
    private Vector3 startPoint;
    private Camera mainCam;
    private int score;


    private float multiplier;
    private BoxCollider2D sector;


    List<GameObject> ActiveBoxes = new List<GameObject>();

    private void Awake()
    {
        if (Instance != null)
            Destroy(this);
        else
            Instance = this;

        box = GetComponent<BoxCollider2D>();
        sector = m_Sector.GetComponent<BoxCollider2D>();
    }

    // Use this for initialization
    void Start()
    {
        GameController.Instance.OnVictoryAction += ResetBoxes;
        Invoke("SetBoxes", 0.3f);
    }

    private void OnDestroy()
    {
        GameController.Instance.OnVictoryAction -= ResetBoxes;
    }

    private void SetBoxes()
    {
        sectorHight = box.size.y / m_SectorCount;
        startPoint.x = box.bounds.center.x;
        startPoint.y = box.bounds.center.y + box.bounds.extents.y - sectorHight * 0.5f;
        startPoint.z = 10f;
        multiplier = box.size.y / (sector.size.y * m_SectorCount);
        Vector3 currentStartPoint = startPoint;
        for (int i = 0; i < m_SectorCount; i++)
        {
            var box = Instantiate(m_Sector, currentStartPoint, m_Sector.transform.rotation);
            ActiveBoxes.Add(box);
            currentStartPoint.y -= sectorHight;
            Vector3 scale = box.transform.localScale;
            scale.y = multiplier;
            box.transform.localScale = scale;
        }
        int nm = UnityEngine.Random.Range(0, ActiveBoxes.Count);
        ActiveBoxes[nm].GetComponent<BoxController>().IsRed = true;
    }

    private void ResetBoxes()
    {
        foreach (var item in ActiveBoxes)
        {
            Destroy(item);
        }
        ActiveBoxes.Clear();
        SetBoxes();
    }
}
