﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxController : MonoBehaviour
{
    [SerializeField]
    private Material m_redMat;

    private bool isRed;
    public bool IsRed
    {
        get { return isRed; }
        set
        {
            if(value)
            {
                GetComponent<SpriteRenderer>().material = m_redMat;
            }
            isRed = value;
        }
    }
}
