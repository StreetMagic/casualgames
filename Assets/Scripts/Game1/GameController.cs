﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
    public static GameController Instance { get; private set; }

    [SerializeField]
    private Text m_ScoreText;

    private int score;
    private Camera mainCam;
    private float timer;

    private int currentBoxesCount = 2;
    private int currentBoxesLifetime = 4;

    private bool isGameOver = false;

    public Action OnVictoryAction;
    public Action OnLoseAction;

    void Awake()
    {
        if (Instance != null)
            Destroy(this);
        else
            Instance = this;
    }

    // Use this for initialization
    void Start()
    {
        BoxManager.Instance.m_SectorCount = currentBoxesCount;
        mainCam = Camera.main;
        InputManager.Instance.SwipeAction += CheckBox;
    }

    private void OnDestroy()
    {
        InputManager.Instance.SwipeAction -= CheckBox;
    }

    private void CheckBox(InputData data)
    {
        if (isGameOver)
            return;

        Ray ray = mainCam.ScreenPointToRay(data.StartPositon);
        RaycastHit2D hit = Physics2D.Raycast(ray.origin, ray.direction);
        if (hit)
        {
            var box = hit.collider.GetComponent<BoxController>();
            if (box)
            {
                if (box.IsRed)
                {
                    UpdateBoxes();
                    if (OnVictoryAction != null)
                        OnVictoryAction();
                    StartCoroutine(TimerCoroutine());

                }
                else
                {
                    Lose();
                }
            }
        }
        else
        { print("nothing!"); }
    }

    private void UpdateBoxes()
    {
        StopAllCoroutines();
        timer = 0;
        score++;
        m_ScoreText.text = score.ToString();
        if (score % 5 == 0)
        {
            currentBoxesLifetime--;
            currentBoxesLifetime = Mathf.Clamp(currentBoxesLifetime, 1, 4);
        }

        if(score%10 == 0)
        {
            currentBoxesCount++;
            currentBoxesCount = Mathf.Clamp(currentBoxesCount, 2, 6);
            BoxManager.Instance.m_SectorCount = currentBoxesCount;
        }
    }

    private void Lose()
    {
        isGameOver = true;
        m_ScoreText.text = "GAME OVER!";
        if (OnLoseAction != null)
            OnLoseAction();
    }

    private IEnumerator TimerCoroutine()
    {
        while (true)
        {
            timer += Time.deltaTime;
            if (timer > currentBoxesLifetime)
                Lose();
            yield return null;
        }
    }
}
