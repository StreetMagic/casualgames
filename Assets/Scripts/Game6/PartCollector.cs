﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PartCollector : MonoBehaviour
{
    public static PartCollector Instance {get; private set;}

    [SerializeField]
    private Text m_ScoreText;

    [SerializeField]
    private Text m_CollectorScoreText;

    private PartColors taskColor = PartColors.white;
    private int taskPartCount;

    public Action OnTaskComplete;
    public Action OnTaskFailed;

    private int finalScore;
    private int thisParts;

    void Awake()
    {
        if (Instance != null)
            Destroy(this);
        else
            Instance = this;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        PartObject obj = collision.gameObject.GetComponent<PartObject>();
        if (obj)
        {
            if (taskColor != obj.color)
            {
                AddPart();
                Destroy(obj.gameObject);
            }
            else
            {
                Destroy(obj.gameObject);
                if (OnTaskFailed != null)
                    OnTaskFailed();
                //thisParts = 0;
                //m_CollectorScoreText.text = thisParts.ToString() + "/" + taskPartCount.ToString();
                m_CollectorScoreText.text = "GAME OVER!";
            }
        }
    }

    private void AddPart()
    {
        thisParts++;
        m_CollectorScoreText.text = thisParts.ToString() + "/" + taskPartCount.ToString();
        if (thisParts >= taskPartCount)
        {
            if (OnTaskComplete != null)
                OnTaskComplete();
            finalScore++;
            m_ScoreText.text = finalScore.ToString();
            thisParts = 0;
            m_CollectorScoreText.text = thisParts.ToString() + "/" + taskPartCount.ToString();
        }
    }

    public void SetCollectorTask(PartColors color, int count)
    {
        taskColor = color;
        taskPartCount = count;
        m_CollectorScoreText.text = thisParts.ToString() + "/" + taskPartCount.ToString();
    }
}
