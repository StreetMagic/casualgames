﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlyerInput : MonoBehaviour
{
    private Camera mainCam;


    // Use this for initialization
    void Start()
    {
        mainCam = Camera.main;
        InputManager.Instance.TapAction += KillPart;
    }

    private void OnDestroy()
    {
        InputManager.Instance.TapAction -= KillPart;
    }

    private void KillPart(InputData data)
    {
        Ray ray = mainCam.ScreenPointToRay(data.StartPositon);
        RaycastHit2D hit = Physics2D.Raycast(ray.origin, ray.direction);
        if (hit)
        {
            var box = hit.collider.GetComponent<PartObject>();
            if (box)
            {
                Destroy(box.gameObject);
            }
        }
    }
}
