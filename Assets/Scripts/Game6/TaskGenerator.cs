﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PartColors { white, red, blue, green, black }
public class TaskGenerator : MonoBehaviour
{

    [SerializeField]
    private Material[] m_Materials;

    [SerializeField]
    private GameObject m_Part;

    [SerializeField]
    private GameObject m_Emmiter;

    [SerializeField]
    private PartCollector m_Collector;

    [SerializeField]
    private float m_GenerationDelay;

    private PartColors currentColor;

    private int taskPartsCount;
    private SpriteRenderer myRenderer;

    // Use this for initialization
    void Start()
    {
        currentColor = PartColors.white;
        myRenderer = GetComponent<SpriteRenderer>();
        m_Collector.OnTaskComplete += GenerateTask;
        m_Collector.OnTaskFailed += Lose;
        Invoke("GenerateTask", 1f);
    }

    private void Lose()
    {
        StopAllCoroutines();
    }

    private void OnDestroy()
    {
        m_Collector.OnTaskComplete -= GenerateTask;
        m_Collector.OnTaskFailed -= Lose;
    }

    private void AssignColor(SpriteRenderer renderer, PartObject part)
    {
        int nm = UnityEngine.Random.Range(0, 4);

        if(nm == 0)
        {
            renderer.material = m_Materials[0];
            part.color = PartColors.white;
        }
        else if(nm ==1)
        {
            renderer.material = m_Materials[1];
            part.color = PartColors.red;
        }
        else if (nm == 2)
        {
            renderer.material = m_Materials[2];
            part.color = PartColors.blue;
        }
        else if (nm == 3)
        {
            renderer.material = m_Materials[3];
            part.color = PartColors.green;
        }
        else if (nm == 4)
        {
            renderer.material = m_Materials[4];
            part.color = PartColors.black;
        }
    }

    private void GenerateTask()
    {
        //taskPartsCount = Random.Range(1, 5);
        taskPartsCount = 10;
        int nm = UnityEngine.Random.Range(0, 4);

        if (nm == 0)
        {
            myRenderer.material = m_Materials[0];
            currentColor = PartColors.white;
        }
        else if (nm == 1)
        {
            myRenderer.material = m_Materials[1];
            currentColor = PartColors.red;
        }
        else if (nm == 2)
        {
            myRenderer.material = m_Materials[2];
            currentColor = PartColors.blue;
        }
        else if (nm == 3)
        {
            myRenderer.material = m_Materials[3];
            currentColor = PartColors.green;
        }
        else if (nm == 4)
        {
            myRenderer.material = m_Materials[4];
            currentColor = PartColors.black;
        }
        m_Collector.SetCollectorTask(currentColor, taskPartsCount);
        StopAllCoroutines();
        StartCoroutine(PartsGenerationCoroutine());
    }

    private void GenerateObject()
    {
        var part = Instantiate(m_Part, m_Emmiter.transform.position, m_Part.transform.rotation);
        PartObject obj = part.GetComponent<PartObject>();
        obj.SetTargetPosition(m_Collector.transform.position);
        AssignColor(part.GetComponent<SpriteRenderer>(), obj);
    }

    private IEnumerator PartsGenerationCoroutine()
    {
        while (true)
        {
            GenerateObject();
            yield return new WaitForSeconds(m_GenerationDelay);
        }
    }
}
