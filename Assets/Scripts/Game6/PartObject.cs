﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PartObject : MonoBehaviour
{
    [SerializeField]
    private float m_Speed;

    public PartColors color = PartColors.white;

    private void Start()
    {
        PartCollector.Instance.OnTaskFailed += Destroy;
    }

    private void OnDestroy()
    {
        PartCollector.Instance.OnTaskFailed -= Destroy;
    }

    public void SetTargetPosition(Vector3 position)
    {
        StartCoroutine(MoveCoroutine(position));
    }

    private IEnumerator MoveCoroutine(Vector3 position)
    {
        Vector3 direction = position - transform.position;
        while (true)
        {
            transform.Translate(direction.normalized * Time.deltaTime * m_Speed);
            yield return null;
        }
    }


    private void Destroy()
    {
        Destroy(gameObject);
    }
}
