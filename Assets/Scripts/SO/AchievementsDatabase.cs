﻿using UnityEngine;
using UnityEngine.Events;

[CreateAssetMenu(fileName = "AchievementsDatabase", menuName = "Data/AchiveData", order = 1)]
public class AchievementsDatabase : ScriptableObject
{
    public AchiveItem[] AchievementDatabase;

    public AchiveItem GetObjectByName(string name)
    {
        for (int i = 0; i < AchievementDatabase.Length; i++)
        {
            if (AchievementDatabase[i].Name == name)
            {
                return AchievementDatabase[i];
            }
        }
        return new AchiveItem();
    }

    public void SetObjectByName(string name, AchiveItem item)
    {
        for (int i = 0; i < AchievementDatabase.Length; i++)
        {
            if (AchievementDatabase[i].Name == name)
            {
                AchievementDatabase[i] = item;
                return;
            }
        }
    }
}

[System.Serializable]
public struct AchiveItem
{
    public string Name;
    public bool IsAchieved;
    public int RewardDiamonds;
    public int RewardBonuses;
    public string RewardSkinName;
    public string Description;
    public AchievementAim Aim;
    public int AimAmount;
}
