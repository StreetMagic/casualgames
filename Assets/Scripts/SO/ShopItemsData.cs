﻿using UnityEngine;
using UnityEngine.Events;

[CreateAssetMenu(fileName = "ShopItemsDatabase", menuName = "Data/ShopData", order = 1)]
public class ShopItemsData : ScriptableObject
{
    public ShopDoubleItem DoubleItem;

    public ShopItem[] ShopDatabase;

    public ShopItem GetObjectByName(string name)
    {
        for (int i = 0; i < ShopDatabase.Length; i++)
        {
            if (ShopDatabase[i].Name == name)
            {
                return ShopDatabase[i];
            }
        }
        return new ShopItem();
    }

    public void SetObjectByName(string name)
    {
        for (int i = 0; i < ShopDatabase.Length; i++)
        {
            if (ShopDatabase[i].Name == name)
            {
                ShopDatabase[i].Owned = true;
                return;
            }
        }
    }
}

[System.Serializable]
public struct ShopItem
{
    public string Name;
    public bool Owned;
    public int Price;
    public Sprite PromoIcon;
    public string Description;
}

[System.Serializable]
public struct ShopDoubleItem
{
    public int DiamondsCount;
    public float DiamondsCooldown;
    public int BonusesCost;
    public int BonusesCount;
}