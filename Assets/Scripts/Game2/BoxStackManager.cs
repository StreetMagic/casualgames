﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxStackManager : MonoBehaviour
{
    public static BoxStackManager Instance { get; private set; }

    [SerializeField]
    private GameObject[] m_Boxes;

    [SerializeField]
    public int m_ActiveBoxesCount;

    [SerializeField]
    public int m_BoxLifetime;

    [SerializeField]
    public int m_ActiveBoxesLimit;

    public List<BoxScript> boxQueue = new List<BoxScript>();
    public List<BoxScript> activeBoxes = new List<BoxScript>();

    void Awake()
    {
        if (Instance != null)
            Destroy(this);
        else
            Instance = this;
    }

    // Use this for initialization
    void Start()
    {
        Game2Controller.Instance.OnVictoryAction += SetBoxes;
        Invoke("SetBoxes", 0.3f);
        StartCoroutine(GameCoroutine());
    }

    private void OnDestroy()
    {
        Game2Controller.Instance.OnVictoryAction -= SetBoxes;
    }

    private void SetBoxes()
    {
        for (int i = 0; i < m_ActiveBoxesCount; i++)
        {
            m_Boxes[i].SetActive(true);
        }
    }

    public void ActivateBox(BoxScript box)
    {
        if(!boxQueue.Contains(box))
        {
            boxQueue.Add(box);
        }
    }

    public void DeactivateBox(BoxScript box)
    {
        if (activeBoxes.Contains(box))
        {
            activeBoxes.Remove(box);
        }
    }

    private IEnumerator GameCoroutine()
    {
        yield return new WaitForSeconds(0.4f);
        while (true)
        {
            if (activeBoxes.Count < m_ActiveBoxesLimit)
            {
                if (boxQueue.Count > 0)
                {
                    BoxScript newBox = boxQueue[0];
                    newBox.ActivateBox();
                    activeBoxes.Add(newBox);
                    boxQueue.Remove(newBox);
                }
            }
            yield return new WaitForSeconds(0.2f);
        }
    }
}
