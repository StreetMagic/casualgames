﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Game2Controller : MonoBehaviour
{
    public static Game2Controller Instance { get; private set; }

    [SerializeField]
    private Text m_ScoreText;

    private int score;
    private Camera mainCam;

    private int currentBoxesCount = 2;
    private int currentBoxesLifetime = 4;

    private bool isGameOver = false;

    public Action OnVictoryAction;
    public Action OnLoseAction;

    void Awake()
    {
        if (Instance != null)
            Destroy(this);
        else
            Instance = this;
    }

    // Use this for initialization
    void Start()
    {
        BoxStackManager.Instance.m_ActiveBoxesCount = currentBoxesCount;
        BoxStackManager.Instance.m_BoxLifetime = currentBoxesLifetime;
        mainCam = Camera.main;
        InputManager.Instance.TapAction += CheckBox;
        OnLoseAction += Lose;
    }

    private void OnDestroy()
    {
        InputManager.Instance.TapAction -= CheckBox;
        OnLoseAction -= Lose;
    }

    private void CheckBox(InputData data)
    {
        if (isGameOver)
            return;

        Ray ray = mainCam.ScreenPointToRay(data.StartPositon);
        RaycastHit2D hit = Physics2D.Raycast(ray.origin, ray.direction);
        if (hit)
        {
            var box = hit.collider.GetComponent<BoxScript>();
            if (box)
            {
                if (box.IsRed)
                {
                    box.DeactivateBox();
                    UpdateBoxes();
                }
                //else
                //{
                //    Lose();
                //}
            }
        }
    }

    private void UpdateBoxes()
    {
        score++;
        m_ScoreText.text = score.ToString();
        if (currentBoxesCount >= 8)
            return;

        switch (currentBoxesCount)
        {
            case 2:
                BoxStackManager.Instance.m_ActiveBoxesLimit = 1;
                break;
            case 4:
                BoxStackManager.Instance.m_ActiveBoxesLimit = 2;
                break;
            case 6:
                BoxStackManager.Instance.m_ActiveBoxesLimit = 3;
                break;
            default:
                break;
        }

        if (score % 5 == 0)
        {
            currentBoxesCount++;
            BoxStackManager.Instance.m_ActiveBoxesCount = currentBoxesCount;
            if (OnVictoryAction != null)
                OnVictoryAction();
        }
    }

    private void Lose()
    {
        if (isGameOver)
            return;

        isGameOver = true;
        m_ScoreText.text = "GAME OVER!";
    }
}
