﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxScript : MonoBehaviour
{
    [SerializeField]
    private float m_lifetime;

    [SerializeField]
    private Material m_redMat;

    [SerializeField]
    private Material m_greyMat;

    public bool IsRed = false;

    private bool isActive;
    private float timer;
    private SpriteRenderer myRenderer;

    private IEnumerator myCoroutine;
    // Use this for initialization
    void Start()
    {
        myRenderer = GetComponent<SpriteRenderer>();
        myCoroutine = TimerCoroutine();
        StartCoroutine(myCoroutine);
        StartCoroutine(StatusCheck());
    }

    private IEnumerator TimerCoroutine()
    {
        float currentTimer = Random.Range(0.5f * m_lifetime, 1.5f * m_lifetime);
        while (true)
        {
            timer += Time.deltaTime;
            if (timer > currentTimer)
            {
                if (IsRed)
                {
                    if (Game2Controller.Instance.OnLoseAction != null)
                        Game2Controller.Instance.OnLoseAction();
                }
                else
                    BoxStackManager.Instance.ActivateBox(this);
                break;
            }
            yield return null;
        }

    }

    public void ActivateBox()
    {
        IsRed = true;
        myRenderer.material = m_redMat;
        timer = 0;
        StopCoroutine(myCoroutine);
        myCoroutine = TimerCoroutine();
        StartCoroutine(myCoroutine);
        StartCoroutine(StatusCheck());
    }

    public void DeactivateBox()
    {
        if (!IsRed)
            return;

        StopCoroutine(myCoroutine);
        timer = 0;
        IsRed = false;
        myRenderer.material = m_greyMat;
        StartCoroutine(TimerCoroutine());
        BoxStackManager.Instance.DeactivateBox(this);
    }

    private IEnumerator StatusCheck()
    {
        while (true)
        {
            if (!IsRed)
                BoxStackManager.Instance.DeactivateBox(this);
            yield return new WaitForSeconds(1f);
        }
    }
}
