﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallsResizer : MonoBehaviour
{
    [SerializeField]
    private GameObject m_leftWall;

    [SerializeField]
    private GameObject m_rightWall;


    private float[] aspects = { 0.5f, 0.5625f, 0.625f, 0.75f };

    private float currentAspect;
    private int aspectNumber;
    // Use this for initialization
    void Start()
    {
        currentAspect = (float)Screen.width / Screen.height;
        FindCLosest();
        SetSize();
    }

    private void Update()
    {
        //currentAspect = (float)Screen.width / Screen.height;
        //FindCLosest();
        //SetSize();
    }

    private void FindCLosest()
    {
        aspectNumber = 0;
        float leftover = Mathf.Abs(aspects[0] - currentAspect);
        for (int i = 0; i < aspects.Length; i++)
        {
            if(Mathf.Abs(aspects[i] - currentAspect) <= leftover)
            {
                leftover = Mathf.Abs(aspects[i] - currentAspect);
                aspectNumber = i;
            }
        }
    }

    private void SetSize()
    {
        Vector3 posLeft = m_leftWall.transform.position;
        Vector3 posRight = m_rightWall.transform.position;
        switch (aspectNumber)
        {
            case 0:
                posLeft.x = -29f;
                m_leftWall.transform.position = posLeft;
                posRight.x = 29f;
                m_rightWall.transform.position = posRight;
                break;
            case 1:
                posLeft.x = -32f;
                m_leftWall.transform.position = posLeft;
                posRight.x = 32f;
                m_rightWall.transform.position = posRight;
                break;
            case 2:
                posLeft.x = -35f;
                m_leftWall.transform.position = posLeft;
                posRight.x = 35f;
                m_rightWall.transform.position = posRight;
                break;
            case 3:
                posLeft.x = -41f;
                m_leftWall.transform.position = posLeft;
                posRight.x = 41f;
                m_rightWall.transform.position = posRight;
                break;
            default:
                break;
        }
    }

}
