﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cloud : MonoBehaviour
{
    [SerializeField]
    private float m_maxHeight;

    [SerializeField]
    private float m_minHeight;

    [SerializeField]
    private float m_minSpeed;

    [SerializeField]
    private float m_maxSpeed;

    [SerializeField]
    private float m_distanceX;

    [SerializeField]
    private bool m_toTheRight;

    private Vector3 startPos;
    private float speed;

    // Use this for initialization
    void Awake()
    {
        startPos = transform.position;
    }

    private void OnEnable()
    {
        Vector3 pos = startPos;
        pos.y = Random.Range(m_minHeight, m_maxHeight);
        transform.position = pos;
        speed = Random.Range(m_minSpeed, m_maxSpeed);
        StartCoroutine(CloudMoveCoroutine());
    }

    private void OnDisable()
    {
        transform.position = startPos;
    }

    private IEnumerator CloudMoveCoroutine()
    {
        if (m_toTheRight)
        {
            while (transform.position.x < m_distanceX)
            {
                transform.Translate(Vector3.right * speed);
                yield return null;
            }
        }
        else
        {
            while (transform.position.x > m_distanceX)
            {
                transform.Translate(Vector3.left * speed);
                yield return null;
            }
        }

        gameObject.SetActive(false);
    }

}
