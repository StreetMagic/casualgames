﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkyRotator : MonoBehaviour
{
    [SerializeField]
    private float m_speed;

    // Update is called once per frame
    void Update()
    {
        if (Time.timeScale == 0)
            return;

        transform.Rotate(Vector3.forward, m_speed);
    }
}
