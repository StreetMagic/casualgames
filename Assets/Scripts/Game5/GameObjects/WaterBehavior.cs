﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterBehavior : MonoBehaviour
{
    [SerializeField]
    private float m_time;

    private float timer;
    // Use this for initialization
    void Start()
    {
        timer = m_time;
    }

    // Update is called once per frame
    void Update()
    {
        if (timer > 0)
            timer -= Time.deltaTime;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (timer <= 0)
        {
            timer = m_time;
            Rigidbody2D rig = collision.collider.GetComponent<Rigidbody2D>();
            if (rig && rig.bodyType != RigidbodyType2D.Static)
            {
                rig.AddForce(Vector2.right * Vector2.up * 100f, ForceMode2D.Impulse);
                rig.AddTorque(3000, ForceMode2D.Impulse);
                print(rig.gameObject.name);
            }
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (timer <= 0)
        {
            timer = m_time;
            Rigidbody2D rig = collision.GetComponent<Rigidbody2D>();
            if (rig && rig.bodyType != RigidbodyType2D.Static)
            {
                rig.AddForce(Vector2.right * Vector2.up * 100f, ForceMode2D.Impulse);
                rig.AddTorque(3000, ForceMode2D.Impulse);
                print(rig.gameObject.name);
            }
        }
    }
}
