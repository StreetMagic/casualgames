﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SunMoonMovement : MonoBehaviour
{
    [SerializeField]
    private float m_speed;

    [SerializeField]
    private float m_fadeSpeed;

    [SerializeField]
    private float m_topEdge;

    [SerializeField]
    private float m_topFadeEdge;

    [SerializeField]
    private float m_botFadeEdge;

    [SerializeField]
    private float m_botEdge;

    public bool isMoveble;
    private bool canMove = true;
    private Vector3 startPos;
    private IEnumerator myCoroutine;
    private SpriteRenderer myRenderer;

    private void OnEnable()
    {
        if (isMoveble)
        {
            startPos = transform.position;
            startPos.y = m_botEdge;
        }
        if (!myRenderer)
            myRenderer = GetComponent<SpriteRenderer>();
        StartCoroutine(AppearCoroutine());
    }

    // Update is called once per frame
    void Update()
    {
        if (Time.timeScale == 0)
            return;

        if (!isMoveble)
            return;

        if (!canMove)
            return;

        transform.Translate(Vector3.up * m_speed);

        if (transform.position.y > m_topEdge)
        {
            canMove = false;
        }

        if (transform.position.y > m_topFadeEdge)
        {
            StartFade();
        }
    }

    public void StartFade()
    {
        if (myCoroutine != null)
            return;

        myCoroutine = DissapearCoroutine();
        StartCoroutine(myCoroutine);
    }

    private IEnumerator DissapearCoroutine()
    {
        float t = myRenderer.color.a;
        Color newColor = myRenderer.color;
        if (isMoveble)
        {
            float distance = Mathf.Abs(m_topEdge - m_topFadeEdge);
            float currentDistance = Mathf.Abs(transform.position.y - m_topFadeEdge);
            while (t > 0)
            {
                currentDistance = Mathf.Abs(transform.position.y - m_topFadeEdge);
                t = 1 - currentDistance / distance;
                newColor.a = t;
                myRenderer.color = newColor;
                yield return null;
            }
            transform.position = startPos;
            canMove = true;
        }
        else
        {
            while (t > 0)
            {
                t -= Time.deltaTime * m_fadeSpeed;
                newColor.a = t;
                myRenderer.color = newColor;
                yield return null;
            }
        }


        gameObject.SetActive(false);
        myCoroutine = null;
    }

    private IEnumerator AppearCoroutine()
    {
        float t = myRenderer.color.a;
        Color newColor = myRenderer.color;

        if (isMoveble)
        {
            float distance = Mathf.Abs(m_botFadeEdge - m_botEdge);
            float currentDistance = Mathf.Abs(transform.position.y - m_botEdge);
            while (t < 1)
            {
                currentDistance = Mathf.Abs(transform.position.y - m_botEdge);
                t = currentDistance / distance;
                newColor.a = t;
                myRenderer.color = newColor;
                yield return null;
            }
        }
        else
        {
            while (t < 1)
            {
                t += Time.deltaTime * m_fadeSpeed;
                newColor.a = t;
                myRenderer.color = newColor;
                yield return null;
            }
        }
    }
}
