﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeatherController : MonoBehaviour
{
    [SerializeField]
    private SunMoonMovement m_sun;

    [SerializeField]
    private SunMoonMovement m_moon;

    [SerializeField]
    private SunMoonMovement m_stars;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.GetComponent<Evening>())
        {
            m_stars.gameObject.SetActive(true);
            m_moon.gameObject.SetActive(true);
        }
        else if (collision.GetComponent<Morning>())
        {
            m_sun.gameObject.SetActive(true);
            if (m_stars.gameObject.activeSelf)
                m_stars.StartFade();
        }
    }
}
