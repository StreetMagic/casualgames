﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloudMaster : MonoBehaviour
{
    [SerializeField]
    private GameObject[] m_clouds;

    [SerializeField]
    private float m_minDelay;

    [SerializeField]
    private float m_maxDelay;

    [SerializeField]
    private int m_maxCloudsAtOnce;

    private float delayTimer;
    private bool canActivateCloud = false;

    // Use this for initialization
    void Start()
    {
        delayTimer = Random.Range(m_minDelay, m_maxDelay);
        StartCoroutine(CloudActivityCoroutine());
    }


    private IEnumerator CloudActivityCoroutine()
    {
        int n = 1;
        while (true)
        {
            if (delayTimer > 0)
            {
                delayTimer -= Time.deltaTime;
                yield return null;
            }
            else
            {
                n = 1;
                //n = Random.Range(1, m_maxCloudsAtOnce);
                int counter = 0;
                while (n > 0)
                {
                    counter++;
                    int i = Random.Range(0, m_clouds.Length);
                    if (!m_clouds[i].activeSelf)
                    {
                        m_clouds[i].SetActive(true);
                        n--;
                    }
                    if (counter > 20)
                        break;
                }
                delayTimer = Random.Range(m_minDelay, m_maxDelay);
                yield return null;
            }
        }
    }
}
