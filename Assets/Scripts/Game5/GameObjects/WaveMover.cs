﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaveMover : MonoBehaviour
{
    [SerializeField]
    private float m_waveSpeed;

    [SerializeField]
    private float m_moveDistance;

    [SerializeField]
    private bool m_moveToTheRight = true;

    [SerializeField]
    private AnimationCurve m_waveSpeedCurve;

    private float leftEdge;
    private float rightEdge;
    private float waveSpeed;
    private float distance;
    float t = 0;

    // Use this for initialization
    void Start()
    {
        leftEdge = transform.position.x - m_moveDistance;
        rightEdge = transform.position.x + m_moveDistance;
        distance = Mathf.Abs(leftEdge - rightEdge);
    }

    // Update is called once per frame
    void Update()
    {
        if (m_moveToTheRight)
            t = Mathf.Abs(transform.position.x - rightEdge) / distance;
        else
            t = Mathf.Abs(transform.position.x - leftEdge) / distance;

        waveSpeed = m_waveSpeedCurve.Evaluate(t);

        if (m_moveToTheRight)
            transform.Translate(Vector3.right * waveSpeed);
        else
            transform.Translate(Vector3.left * waveSpeed);
        if (m_moveToTheRight && transform.position.x > rightEdge)
            m_moveToTheRight = false;
        if (!m_moveToTheRight && transform.position.x < leftEdge)
            m_moveToTheRight = true;
    }
}