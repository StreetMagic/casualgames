﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewFieldObject : FieldObject
{
    [SerializeField]
    private Sprite[] m_spriteBase;

    [SerializeField]
    private GameObject m_DestroyEffect;

    [SerializeField]
    private Color m_baseColor;

    [SerializeField]
    private Color m_inactiveColor;

    [SerializeField]
    private Color m_stabilizationColor;

    [SerializeField]
    private SpriteRenderer m_renderer;

    [SerializeField]
    private bool useColorSwap;

    // Use this for initialization
    void Start()
    {
        myRig = GetComponent<Rigidbody2D>();
        myCollider = GetComponent<Collider2D>();
        myRenderer = m_renderer;
        if (m_spriteBase.Length > 1)
        {
            int nm = Random.Range(0, m_spriteBase.Length - 1);
            myRenderer.sprite = m_spriteBase[nm];
        }

        if (useColorSwap)
            myRenderer.color = m_baseColor;
        if (FieldController.Instance)
        {
            if (isActive)
                FieldController.Instance.AddToActiveList(this);
            else
                OnLoadSettleDown();

        }
        else if (FakeFieldController.FakeInstance)
            FakeFieldController.FakeInstance.AddToActiveList(this);
    }

    override protected IEnumerator StabilizationCoroutine()
    {
        Color baseColor = myRenderer.color;
        float flickingTimer = 0.2f;
        float stabilazationTimer = 1f;
        while (myRig.velocity == Vector2.zero)
        {
            myRenderer.color = m_stabilizationColor;
            stabilazationTimer -= Time.deltaTime;
            flickingTimer -= Time.deltaTime;
            if (flickingTimer <= 0)
            {
                if (useColorSwap)
                {
                    if (myRenderer.color == m_baseColor)
                        myRenderer.color = m_stabilizationColor;
                    else myRenderer.color = m_baseColor;
                }
                else
                {
                    if (myRenderer.color == baseColor)
                        myRenderer.color = m_stabilizationColor;
                    else myRenderer.color = baseColor;
                }
                flickingTimer = 0.2f;
            }

            if (stabilazationTimer <= 0)
            {
                SettleObjectDown();
                StopCoroutine(myCoroutine);
                myCoroutine = null;
            }
            yield return null;
        }

        if (useColorSwap)
            myRenderer.color = m_baseColor;
        else
            myRenderer.color = baseColor;

        myCoroutine = null;

    }

    override protected void SettleObjectDown()
    {
        if (m_BlackSprite)
        {
            myRenderer.color = Color.white;
            myRenderer.sprite = m_BlackSprite;
        }
        else
            myRenderer.color = m_inactiveColor;

        gameObject.layer = 4;
        if (FieldController.Instance)
        {
            FieldController.Instance.RemoveFromList(this);
            FieldController.Instance.AddToInactiveList(this, true);
        }
        else if (FakeFieldController.FakeInstance)
        {
            FakeFieldController.FakeInstance.RemoveFromList(this);
            FakeFieldController.FakeInstance.AddToInactiveList(this, false);
        }
        myRig.bodyType = RigidbodyType2D.Static;

        if (transform.position.y >= 18f)
        {
            if (FieldController.Instance)
                FieldController.Instance.Invoke("DestroyColumn", 0.5f);
            else if (FakeFieldController.FakeInstance)
                FakeFieldController.FakeInstance.Invoke("DestroyColumn", 0.5f);
        }

        if (m_SettleEffect)
        {
            Vector3 pos = transform.position;
            pos.y = myCollider.bounds.min.y;
            pos.z = -1f;
            GameObject formula = Instantiate(m_SettleEffect, pos, m_SettleEffect.transform.rotation);
            Destroy(formula, 1.5f);
        }

        if (ScoreManager.Instance)
            ScoreManager.Instance.AddScore(1);

        isActive = false;
    }

    private void OnLoadSettleDown()
    {
        if (m_BlackSprite)
            myRenderer.sprite = m_BlackSprite;
        else
            myRenderer.color = m_inactiveColor;
        gameObject.layer = 4;
        FieldController.Instance.AddToInactiveList(this, false);
        myRig.bodyType = RigidbodyType2D.Static;
    }

    override public void DestroyFieldObject()
    {
        if (m_DestroyEffect)
        {
            GameObject destroyEffect = Instantiate(m_DestroyEffect, transform.position, m_DestroyEffect.transform.rotation);
            Destroy(destroyEffect, 0.5f);
        }
    }
}
