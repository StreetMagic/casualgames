﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FieldObject : MonoBehaviour
{
    [SerializeField]
    protected float m_force;

    [SerializeField]
    protected Sprite m_BlackSprite;

    [SerializeField]
    private Material m_BlackMat;

    [SerializeField]
    private Material m_GreyMat;

    [SerializeField]
    protected LayerMask m_mask;

    [Range(0, 5)]
    [SerializeField]
    protected float m_minVelocity;

    [Range(0, 30)]
    [SerializeField]
    protected float m_minAngle;

    [SerializeField]
    protected float m_GoodAngle;

    [SerializeField]
    protected GameObject m_hitEffect;

    [SerializeField]
    protected GameObject m_SettleEffect;

    [SerializeField]
    protected GameObject m_bottomHitEffect;

    protected Rigidbody2D myRig;
    protected SpriteRenderer myRenderer;
    public bool isActive = true;
    protected Material startMaterial;
    protected IEnumerator myCoroutine;
    protected Collider2D myCollider;

    void Start()
    {
        myRig = GetComponent<Rigidbody2D>();
        myCollider = GetComponent<Collider2D>();
        myRenderer = GetComponent<SpriteRenderer>();
        startMaterial = myRenderer.material;
        FieldController.Instance.AddToActiveList(this);
    }

    public void AddForce(Vector2 direction)
    {
        myRig.AddForce(Vector2.up * m_force + direction * m_force * 0.3f, ForceMode2D.Impulse);
    }

    protected void OnCollisionEnter2D(Collision2D collision)
    {
        Color newColor;

        if(m_bottomHitEffect)
        {
            if(collision.gameObject.tag == "Bottom")
            {
                GameObject newEffect = Instantiate(m_bottomHitEffect,
                    collision.contacts[Random.Range(0, collision.contacts.Length - 1)].point, Quaternion.identity);
                Destroy(newEffect, 0.5f);
                return;
            }
        }


        if (m_hitEffect)
        {
            GameObject newEffect = Instantiate(m_hitEffect,
                collision.contacts[Random.Range(0, collision.contacts.Length - 1)].point, Quaternion.identity);

            SpriteRenderer collisionRenderer = collision.gameObject.GetComponent<SpriteRenderer>();
            if (collisionRenderer)
            {
                newColor = collisionRenderer.color;
                if (newEffect.GetComponent<EffectColorChanger>())
                    newEffect.GetComponent<EffectColorChanger>().ChangeColor(newColor);
            }

            else
            {
                collisionRenderer = collision.gameObject.GetComponentInChildren<SpriteRenderer>();
                if (collisionRenderer)
                {
                    newColor = collisionRenderer.color;
                    if (newEffect.GetComponent<EffectColorChanger>())
                        newEffect.GetComponent<EffectColorChanger>().ChangeColor(newColor);
                }
            }
            Destroy(newEffect, 0.5f);
        }
    }

    protected void OnCollisionStay2D(Collision2D collision)
    {
        if (myCoroutine != null)
            return;

        if (!isActive)
            return;

        if (myRig.velocity.sqrMagnitude > m_minVelocity * m_minVelocity)
            return;

        if (collision.gameObject.layer != 4)
            return;

        if (Physics2D.Raycast(transform.position, Vector2.down, 10f, m_mask))
        {
            if (myRig.velocity == Vector2.zero)
            {
                if (myCoroutine == null)
                {
                    myCoroutine = StabilizationCoroutine();
                    StartCoroutine(myCoroutine);
                }
            }
            //angle check
            float angle = Mathf.Abs(transform.rotation.eulerAngles.z % m_GoodAngle);
            if (angle > m_minAngle && angle < m_GoodAngle - m_minAngle)
                return;

            //angle correction
            float currentGoodAngle = transform.rotation.eulerAngles.z >= 0 ? m_GoodAngle : -m_GoodAngle;
            float AngleZ = angle < m_minAngle ? transform.rotation.eulerAngles.z - transform.rotation.eulerAngles.z % currentGoodAngle :
                transform.rotation.eulerAngles.z + currentGoodAngle - transform.rotation.eulerAngles.z % currentGoodAngle;
            transform.rotation = Quaternion.Euler(0, 0, AngleZ);

            SettleObjectDown();
        }
    }

    virtual protected IEnumerator StabilizationCoroutine()
    {
        float flickingTimer = 0.2f;
        float stabilazationTimer = 1f;
        while (myRig.velocity == Vector2.zero)
        {
            myRenderer.material = m_GreyMat;
            stabilazationTimer -= Time.deltaTime;
            flickingTimer -= Time.deltaTime;
            if (flickingTimer <= 0)
            {
                if (myRenderer.material == startMaterial)
                    myRenderer.material = m_GreyMat;
                else myRenderer.material = startMaterial;
                flickingTimer = 0.2f;
            }

            if (stabilazationTimer <= 0)
            {
                SettleObjectDown();
                StopCoroutine(myCoroutine);
                myCoroutine = null;
            }
            yield return null;
        }

        myRenderer.material = startMaterial;
        myCoroutine = null;

    }

    virtual protected void SettleObjectDown()
    {
        myRenderer.material = startMaterial;

        if (m_BlackSprite)
            myRenderer.sprite = m_BlackSprite;
        else
            myRenderer.material = m_BlackMat;

        gameObject.layer = 4;
        FieldController.Instance.RemoveFromList(this);
        FieldController.Instance.AddToInactiveList(this, true);
        myRig.bodyType = RigidbodyType2D.Static;

        if (transform.position.y >= 18f)
        {
            FieldController.Instance.Invoke("DestroyColumn", 0.5f);
        }
        else
        {
            if (m_SettleEffect)
            {
                Vector3 pos = transform.position;
                pos.y = myCollider.bounds.min.y;
                GameObject newEffect = Instantiate(m_SettleEffect, transform.position, Quaternion.identity);
                Destroy(newEffect, 0.5f);
            }
        }
        isActive = false;
    }

    virtual public void DestroyFieldObject()
    {

    }
}
