﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UFOBehavior : MonoBehaviour
{
    [SerializeField]
    private float m_startPoint;

    [SerializeField]
    private float m_endPoint;

    [SerializeField]
    private float m_speed;

    [SerializeField]
    private float m_dissapearSpeed;

    [SerializeField]
    private GameObject m_UFOEffect;

    private List<FieldObject> UFOTargets = new List<FieldObject>();

    // Use this for initialization
    void Start()
    {
        FieldController.Instance.OnDestroyColumn += ActivateUFO;
    }

    private void ActivateUFO()
    {
        StartCoroutine(UFOCoroutine());
    }

    private IEnumerator UFOCoroutine()
    {
        while (transform.position.y > m_endPoint)
        {
            transform.Translate(Vector3.down * m_speed);
            yield return null;
        }

        GameObject effect = Instantiate(m_UFOEffect);
        Destroy(effect, 4f);

        // move cubes
        float timer = 0;
        UFOTargets = FieldController.Instance.GetInactiveObjects();

        while (UFOTargets.Count > 0)
        {
            if (UFOTargets.Count > 0)
            {
                foreach (var item in UFOTargets)
                {
                    Vector3 pos = Vector3.Lerp(item.transform.position, transform.position, Time.deltaTime* m_dissapearSpeed);
                    item.transform.position = pos;
                    //item.transform.Translate((transform.position - item.transform.position).normalized * m_dissapearSpeed);
                    item.transform.localScale = 0.94f * item.transform.localScale;
                }
            }

            if (timer > 0.3f)
            {
                FieldController.Instance.canDestroyObjects = true;
            }
            else
                timer += Time.deltaTime;

            UFOTargets = FieldController.Instance.GetInactiveObjects();
            yield return null;
        }

        // move UFO

        while (transform.position.y < m_startPoint)
        {
            transform.Translate(Vector3.up * m_speed);
            yield return null;
        }

    }
}
