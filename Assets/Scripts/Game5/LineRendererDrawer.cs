﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LineRendererDrawer : MonoBehaviour
{
    [SerializeField]
    private float m_drawSpeed;

    private LineRenderer myLine;
    private PolygonCollider2D myCollider;

    // Use this for initialization
    void Start()
    {
        myLine = GetComponent<LineRenderer>();
        myCollider = GetComponent<PolygonCollider2D>();
        StartCoroutine(LineDrawerCoroutine());
    }

    private void PrepareLine()
    {
        myLine.positionCount = myCollider.points.Length;
        for (int i = 0; i < myCollider.points.Length; i++)
        {
            myLine.SetPosition(i, myCollider.points[i]);
        }
    }

    private IEnumerator LineDrawerCoroutine()
    {
        yield return new WaitForSeconds(2f);

        myLine.SetPosition(0, myCollider.points[0]);
        float t = 0;
        int nm = 0;
        for (int i = 0; i < myCollider.points.Length; i++)
        {
            t = 0;
            if (i != 0)
                myLine.positionCount++;
            if (i + 1 == myCollider.points.Length)
            {
                nm = 0;
            }
            else nm = i + 1;

            while (t < 1)
            {
                t += Time.deltaTime / m_drawSpeed;

                Vector2 destination = Vector2.Lerp(myLine.GetPosition(i), myCollider.points[nm], t);
                myLine.SetPosition(i + 1, destination);
                yield return null;
            }
        }
        myLine.loop = true;
    }
}
