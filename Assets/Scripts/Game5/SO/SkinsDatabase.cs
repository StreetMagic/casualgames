﻿using System;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "FieldObjectsData", menuName = "Data/FieldObjectData", order = 1)]
public class SkinsDatabase : ScriptableObject
{

    public FieldObjectsData[] SkinDatabase;

    public FieldObjectsData GetObjectByName(string skin)
    {
        for (int i = 0; i < SkinDatabase.Length; i++)
        {
            if (SkinDatabase[i].SkinName == skin)
            {
                return SkinDatabase[i];
            }
        }
        return null;
    }
}

[Serializable]
public class FieldObjectsData
{
    public string SkinName;
    public GameObject Background;
    public GameObject[] FieldObjects;
}