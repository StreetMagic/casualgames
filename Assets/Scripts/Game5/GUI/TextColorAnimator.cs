﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextColorAnimator : MonoBehaviour
{
    private Text myText;

    [SerializeField]
    private float m_speed;

    [SerializeField]
    private Color m_goodColor;

    [SerializeField]
    private Color m_badColor;

    private IEnumerator myCoroutine;

    // Use this for initialization
    void Start()
    {
        myText = GetComponent<Text>();
    }

    public void ColorFlick()
    {
        if (myCoroutine != null)
            return;

        myCoroutine = ColorChangeCoroutine();
        StartCoroutine(myCoroutine);
    }

    private IEnumerator ColorChangeCoroutine()
    {
        float t = 0;
        Color startColor = myText.color;
        while (t < 1)
        {
            t += Time.deltaTime * m_speed;
            myText.color = Color.Lerp(startColor, m_goodColor, t);
            yield return null;
        }
        while (t > 0)
        {
            t -= Time.deltaTime * m_speed;
            myText.color = Color.Lerp(m_goodColor, startColor, t);
            yield return null;
        }
        myCoroutine = null;
    }
}
