﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FadeBackOnTap : MonoBehaviour
{
    [SerializeField]
    private float m_maxAlpha;
    [SerializeField]
    private float m_speed;
    [SerializeField]
    private SpriteRenderer m_leftSide;
    [SerializeField]
    private SpriteRenderer m_rightSide;

    public bool isActive = false;

    private Camera mainCam;
    private IEnumerator myCoroutine;
    private SpriteRenderer activePart;

    // Use this for initialization
    void Start()
    {
        mainCam = Camera.main;
        InputManager.Instance.TapAction += FadeOnTap;
    }

    private void OnDestroy()
    {
        InputManager.Instance.TapAction -= FadeOnTap;
    }

    private void FadeOnTap(InputData data)
    {
        if (!isActive)
            return;

        Vector3 pos = mainCam.ScreenToWorldPoint(data.StartPositon);
        if (pos.y > 40f || pos.y < -40f)
            return;

        if (myCoroutine != null)
        {
            StopCoroutine(myCoroutine);
            Color color = activePart.color;
            color.a = m_maxAlpha;
            activePart.color = color;
        }

        if (pos.x > 0)
            activePart = m_rightSide;
        else
            activePart = m_leftSide;

        StartCoroutine(FadeCoroutine());
    }

    private IEnumerator FadeCoroutine()
    {
        float a = activePart.color.a;
        Color currentColor = activePart.color;

        while (a > 0)
        {
            a -= Time.deltaTime * m_speed;
            currentColor.a = a;
            activePart.color = currentColor;
            yield return null;
        }

        while (a < m_maxAlpha)
        {
            a += Time.deltaTime * m_speed;
            currentColor.a = a;
            activePart.color = currentColor;
            yield return null;
        }

        currentColor.a = m_maxAlpha;
        activePart.color = currentColor;
        myCoroutine = null;
    }

    public void TurnOffFadeOnTap()
    {
        isActive = !isActive;
    }
}
