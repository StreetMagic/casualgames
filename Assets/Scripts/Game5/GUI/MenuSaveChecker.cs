﻿using System.IO;
using UnityEngine;

public class MenuSaveChecker : MonoBehaviour
{
    [SerializeField]
    private MainMenuController m_MenuController;

    private SaveData saveData;
    private string path;

    private void Awake()
    {
        path = Application.persistentDataPath + "/SavedProgress.json";
        //path = "Assets/Resources/SavedProgress.json";

        if (File.Exists(path))
        {
            saveData = JsonUtility.FromJson<SaveData>(File.ReadAllText(path));
        }
        else
        {
            TextAsset asset = Resources.Load<TextAsset>("SavedProgress");
            saveData = JsonUtility.FromJson<SaveData>(asset.ToString());
        }
    }

    private void Start()
    {
        if (saveData.isSaved)
            m_MenuController.SetTapText("Tap to continue");
    }

    public void ResetSave()
    {
        saveData = new SaveData();
        saveData.isSaved = false;
        string dataAsText = JsonUtility.ToJson(saveData);
        File.WriteAllText(path, dataAsText);
    }
}
