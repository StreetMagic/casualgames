﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameOverPanel : MonoBehaviour
{
    [SerializeField]
    private Text m_scoreHeaderText;

    [SerializeField]
    private Text m_scoreText;

    [SerializeField]
    private Text m_bestScoreText;

    [SerializeField]
    private Text m_diamondsText;

    [SerializeField]
    private Button m_diamondsButton;

    [SerializeField]
    private Text m_diamondButtonText;

    [SerializeField]
    private Text m_finalDiamondsText;

    [SerializeField]
    private GameObject m_gameOverBack;

    private bool canContinue = false;

    private bool isShown = false;

    private HorizontalLayoutGroup diamondsGroup;

    private void Start()
    {
        diamondsGroup = m_diamondsText.transform.parent.GetComponent<HorizontalLayoutGroup>();
        SetScore();
    }

    public void UpdateDiamonds()
    {
        CurrencyManager.Instance.AddCoins(CurrencyManager.Instance.BonusDiamonds);
        m_diamondsButton.gameObject.SetActive(false);
        m_diamondsText.transform.parent.gameObject.SetActive(false);
        m_finalDiamondsText.transform.parent.gameObject.SetActive(true);
        m_finalDiamondsText.text = (CurrencyManager.Instance.GetRoundCurrency() + CurrencyManager.Instance.BonusDiamonds).ToString();
        LayoutRebuilder.ForceRebuildLayoutImmediate((RectTransform)m_finalDiamondsText.transform.parent);
    }

    public void MainMenu()
    {

        if (!canContinue && ShowInterstitialScript.Instance)
        {
            if (IronSource.Agent.isInterstitialReady())
            {
                ShowInterstitialScript.Instance.ShowInterstitialButtonClicked();
                IronSourceEvents.onInterstitialAdClosedEvent += MainMenu;
                canContinue = true;
                m_gameOverBack.SetActive(true);
                return;
            }
        }
        canContinue = true;

        if (canContinue)
            SceneManager.LoadScene("MainMenu");
    }

    public void RestartGame()
    {
        if (!canContinue && ShowInterstitialScript.Instance)
        {
            if (IronSource.Agent.isInterstitialReady())
            {
                ShowInterstitialScript.Instance.ShowInterstitialButtonClicked();
                IronSourceEvents.onInterstitialAdClosedEvent += RestartGame;
                canContinue = true;
                m_gameOverBack.SetActive(true);
                return;
            }
        }
        canContinue = true;

        if (canContinue)
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void SetScore()
    {
        int bestScore = PlayerPrefs.GetInt("HighestScore", 0);
        if (bestScore < ScoreManager.Instance.CurrentScore)
        {
            m_scoreHeaderText.text = "NEW BEST SCORE!";
            m_scoreText.text = ScoreManager.Instance.CurrentScore.ToString();
            PlayerPrefs.SetInt("HighestScore", ScoreManager.Instance.CurrentScore);
            m_bestScoreText.gameObject.SetActive(false);
        }
        else
        {
            m_scoreText.text = ScoreManager.Instance.CurrentScore.ToString();
            m_bestScoreText.gameObject.SetActive(true);
            m_bestScoreText.text = "BEST SCORE: " + bestScore.ToString();
        }
        CountDiamonds();
    }

    private void CountDiamonds()
    {
        int roundDiamonds = CurrencyManager.Instance.GetRoundCurrency();
        m_diamondsText.text = roundDiamonds.ToString();
        int bonus = CurrencyManager.Instance.CountBonusCurrency();
        m_diamondButtonText.text = "+" + bonus;
        diamondsGroup.childAlignment = TextAnchor.MiddleLeft;
        LayoutRebuilder.ForceRebuildLayoutImmediate((RectTransform)m_diamondsText.transform.parent);
    }

    public void ShowAdvForDiamonds()
    {
        if (isShown)
            return;

        if (IronSource.Agent.isRewardedVideoAvailable())
        {
            IronSource.Agent.showRewardedVideo("ExtraDiamonds");
            isShown = true;
            Invoke("UpdateDiamonds", 0.5f);
        }
    }
}
