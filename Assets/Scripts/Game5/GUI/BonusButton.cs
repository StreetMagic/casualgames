﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BonusButton : MonoBehaviour
{
    [SerializeField]
    private GameObject m_adImage;

    [SerializeField]
    private Text m_bonusText;

    private Button myButton;

    private void Awake()
    {
        myButton = GetComponent<Button>();
    }

    private void Start()
    {
        CheckBonusCount();
    }

    private void CheckBonusCount()
    {
        if (CurrencyManager.Instance.CurrentBonusCount <= 0)
        {
            m_adImage.SetActive(true);
            myButton.onClick.RemoveAllListeners();
            myButton.onClick.AddListener(delegate { ShowAdv(); });
            m_bonusText.text = "";
        }
        else
        {
            m_adImage.SetActive(false);
            myButton.onClick.RemoveAllListeners();
            myButton.onClick.AddListener(delegate { UseBonus(); });
            m_bonusText.text = CurrencyManager.Instance.CurrentBonusCount.ToString();
        }
    }

    private void ShowAdv()
    {
        if (IronSource.Agent.isRewardedVideoAvailable())
        {
            IronSourceEvents.onRewardedVideoAdClosedEvent += AddBonus;
            IronSource.Agent.showRewardedVideo();
        }
    }

    private void UseBonus()
    {
        CurrencyManager.Instance.AddActiveBonuses(-1);
        FieldController.Instance.DestroyTopItem();
        CheckBonusCount();
    }

    private void AddBonus()
    {
        CurrencyManager.Instance.AddActiveBonuses(1);
        CheckBonusCount();
        IronSourceEvents.onRewardedVideoAdClosedEvent -= AddBonus;
    }
}
