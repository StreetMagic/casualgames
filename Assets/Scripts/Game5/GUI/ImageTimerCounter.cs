﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ImageTimerCounter : MonoBehaviour
{
    [SerializeField]
    private float m_time;

    [SerializeField]
    private GameObject m_timeOutPanel;

    private Image myImage;
    // Use this for initialization
    void Start()
    {
        myImage = GetComponent<Image>();
        myImage.fillAmount = 1;
    }

    private void OnDisable()
    {
        myImage.fillAmount = 1;
    }

    // Update is called once per frame
    void Update()
    {
        if (myImage.fillAmount > 0)
        {
            myImage.fillAmount -= Time.deltaTime / m_time;
            if (myImage.fillAmount <= 0)
            {
                GameMenuUIManager.Instance.GameOver();
                m_timeOutPanel.SetActive(false);
            }
        }
    }


}
