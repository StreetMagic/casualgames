﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LerpTextColor : MonoBehaviour
{
    [SerializeField]
    private Color m_minColor;

    [SerializeField]
    private Color m_middleColor;

    [SerializeField]
    private Color m_maxColor;

    [SerializeField]
    private float m_minAmount;

    [SerializeField]
    private float m_maxAmount;

    [SerializeField]
    private float m_middleAmount;

    private Text myText;
    // Use this for initialization
    void Start()
    {
        myText = GetComponent<Text>();
        StartCoroutine(UpdateColorCoroutine());
    }

    private IEnumerator UpdateColorCoroutine()
    {
        //float t;
        while (true)
        {
            if (TimeManager.Instance.Timer < m_minAmount)
                myText.color = m_minColor;
            else if (TimeManager.Instance.Timer < m_middleAmount)
                myText.color = m_middleColor;
            else
                myText.color = m_maxColor;
            //t = TimeManager.Instance.Timer / m_maxAmount;
            //myText.color = Color.Lerp(m_minColor, m_maxColor, t);
            yield return new WaitForSeconds(0.5f);
        }
    }
}
