﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenuController : MonoBehaviour
{
    public static MainMenuController Instance { get; set; }

    [SerializeField]
    private GameObject m_shop;

    [SerializeField]
    private GameObject m_achivePanel;

    [SerializeField]
    private Transform m_shopRoot;

    [SerializeField]
    private Text m_bestScoreText;

    [SerializeField]
    private Text m_TapToStartText;

    [SerializeField]
    private FakePlayer m_player;

    private GameObject activeShop;
    private GameObject activeAchievementsPanel;

    private void Awake()
    {
        if (Instance != null)
            Destroy(this);
        else
            Instance = this;
    }

    // Use this for initialization
    void Start()
    {
        m_bestScoreText.text = "Best score: " + PlayerPrefs.GetInt("HighestScore", 0).ToString();
    }

    public void SetTapText(string newText)
    {
        m_TapToStartText.text = newText;
    }

    public void OpenShop()
    {
        if (activeShop != null)
            return;

        activeShop = Instantiate(m_shop, m_shopRoot);
        m_player.StopFakePlayer();
    }

    public void CloseShop()
    {
        Destroy(activeShop);
        m_player.RestartGame();
    }

    public void OpenAchievementsPanel()
    {
        if (activeAchievementsPanel != null)
            return;

        activeAchievementsPanel = Instantiate(m_achivePanel, m_shopRoot);
        m_player.StopFakePlayer();
    }

    public void CloseAchievementsPanel()
    {
        Destroy(activeAchievementsPanel);
        m_player.RestartGame();
    }
}
