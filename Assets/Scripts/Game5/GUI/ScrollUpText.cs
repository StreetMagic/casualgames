﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScrollUpText : MonoBehaviour
{
    [SerializeField]
    private Text m_BonusScoreText;

    private Color baseColor;
    private Color newColor;

    public static ScrollUpText GetScrollUpText(string text, Vector3 position)
    {
        ScrollUpText newText = null;
        GameObject scrollText = Instantiate((Resources.Load("ScrollUpText") as GameObject), position, Quaternion.identity, GameMenuUIManager.Instance.transform);
        scrollText.transform.localScale = Vector3.one;
        newText = scrollText.GetComponent<ScrollUpText>();
        newText.SetText(text);

        return newText;
    }

    public static ScrollUpText GetScrollUpText(string text, Vector3 position, Color color)
    {
        ScrollUpText newText = null;
        if (!GameMenuUIManager.Instance)
            return null;

        GameObject scrollText = Instantiate((Resources.Load("ScrollUpText") as GameObject), position, Quaternion.identity, GameMenuUIManager.Instance.transform);
        scrollText.transform.localScale = Vector3.one;
        newText = scrollText.GetComponent<ScrollUpText>();
        newText.SetText(text, color);

        return newText;
    }

    public void SetText(string text)
    {
        m_BonusScoreText.text = text;
    }

    public void SetText(string text, Color color)
    {
        m_BonusScoreText.text = text;
        newColor = color;
    }

    private void Awake()
    {
        baseColor = newColor = m_BonusScoreText.color;
    }

    private void LateUpdate()
    {
        if (baseColor != newColor)
        {
            float a = m_BonusScoreText.color.a;
            newColor.a = a;
            m_BonusScoreText.color = newColor;
        }
    }
}
