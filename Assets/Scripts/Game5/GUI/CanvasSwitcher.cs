﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanvasSwitcher : MonoBehaviour
{
    [SerializeField]
    private GameObject m_mathCanvas;

    [SerializeField]
    private GameObject m_otherCanvas;

    [SerializeField]
    private bool m_switch;

    private void Awake()
    {
        if (!m_switch)
            return;

        if(PlayerPrefs.GetString("ActiveSkinName") == "Math")
        {
            m_otherCanvas.SetActive(false);
            m_mathCanvas.SetActive(true);
        }
    }
}
