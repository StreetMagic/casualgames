﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameMenuUIManager : MonoBehaviour
{
    public static GameMenuUIManager Instance { get; private set; }

    [SerializeField]
    private Text m_timerText;

    [SerializeField]
    private GameObject m_timerTextEffect;

    [SerializeField]
    private GameObject m_ScoreEffect;

    [SerializeField]
    private Color m_PositiveEffectColor;

    [SerializeField]
    private Color m_NegativeEffectColor;

    [SerializeField]
    private Text m_scoreText;

    [SerializeField]
    private GameObject m_settingsPanel;

    [SerializeField]
    private GameObject m_gameOverPanel;

    [SerializeField]
    private GameObject m_timeOutPanel;

    [SerializeField]
    private GameObject m_tutorPanel;

    [SerializeField]
    private GameObject m_pausePanel;

    private Vector3 timerEffectPos;

    void Awake()
    {
        if (Instance != null)
            Destroy(this);
        else
            Instance = this;
        //PlayerPrefs.SetInt("TutorialComplete", 0);
    }

    private void Start()
    {
        if (PlayerPrefs.GetInt("TutorialComplete", 0) == 0)
        {
            m_tutorPanel.SetActive(true);
        }
        else
            Destroy(m_tutorPanel);
        FieldController.Instance.OnGameOverAction += GameOverPanelOpen;
        InputManager.Instance.SwipeAction += PauseMenu;
        //timerEffectPos = Camera.main.WorldToScreenPoint(m_scoreText.transform.position);
        timerEffectPos = m_timerText.transform.position;
        timerEffectPos.z = 0f;
    }

    private void OnDestroy()
    {
        FieldController.Instance.OnGameOverAction -= GameOverPanelOpen;
        InputManager.Instance.SwipeAction -= PauseMenu;
    }

    private void PauseMenu(InputData data)
    {
        Vector3 swipe = data.EndPosition - data.StartPositon;
        if (swipe.magnitude < Screen.height * 0.7f)
            return;

        PauseMenuOpen();
    }

    public void UpdateScore(int score)
    {
        m_scoreText.text = score.ToString();
        if (m_ScoreEffect)
        {
            ParticleEffectPLay();
            //Invoke("ParticleEffectPLay", 0.15f);
        }
    }

    public void UpdateTimer(string time)
    {
        m_timerText.text = time;
    }

    public void PauseMenuOpen()
    {
        Time.timeScale = 0;
        m_settingsPanel.SetActive(true);
        FieldController.Instance.canPush = false;
    }

    public void PauseMenuClose()
    {
        Time.timeScale = 1;
        m_settingsPanel.SetActive(false);
        Invoke("AllowTaps", 0.1f);
    }

    private void GameOverPanelOpen()
    {
        m_gameOverPanel.SetActive(true);
    }

    public void ResetScene(int number)
    {
        Time.timeScale = 1;
        ProgressManager.Instance.ResetSave();
        SceneManager.LoadScene(number);
    }

    private void AllowTaps()
    {
        FieldController.Instance.canPush = true;
    }

    public void MainMenu()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene("MainMenu");
    }

    public void OpenPauseMenu()
    {
        Time.timeScale = 0;
        m_pausePanel.SetActive(true);
    }

    public void ClosePauseMenu()
    {
        Time.timeScale = 1;
        m_pausePanel.SetActive(false);
    }

    public void AnimateTimer(bool isPositive)
    {
        if (!m_timerTextEffect)
            return;

        GameObject effect = Instantiate(m_timerTextEffect, timerEffectPos, m_timerTextEffect.transform.rotation);
        if (isPositive)
            effect.GetComponent<EffectColorChanger>().ChangeColor(m_PositiveEffectColor);
        else
            effect.GetComponent<EffectColorChanger>().ChangeColor(m_NegativeEffectColor);

        Destroy(effect, 0.5f);
    }

    public void OpenTimeOutMenu()
    {
        m_timeOutPanel.SetActive(true);
    }

    public void CloseTimeOutMenu()
    {
        m_timeOutPanel.SetActive(false);
        GameOver();
    }

    public void GameOver()
    {
        if (!FieldController.Instance.GameIsOver)
        {
            FieldController.Instance.GameIsOver = true;
            if (FieldController.Instance.OnGameOverAction != null)
                FieldController.Instance.OnGameOverAction();
        }
    }

    private void ParticleEffectPLay()
    {
        if (!m_ScoreEffect)
            return;

        GameObject scoreEffect = Instantiate(m_ScoreEffect);
        Destroy(scoreEffect, 1f);

    }

    public void CompleteTutor()
    {
        PlayerPrefs.SetInt("TutorialComplete", 1);
        Destroy(m_tutorPanel);
    }
}
