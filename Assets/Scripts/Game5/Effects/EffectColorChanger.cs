﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EffectColorChanger : MonoBehaviour
{
    [SerializeField]
    private ParticleSystem emmiter;

    public void ChangeColor(Color newColor)
    {
        ParticleSystem.MainModule main = emmiter.main;
        main.startColor = newColor;
    }
}