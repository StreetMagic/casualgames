﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EffectMaterialChanger : MonoBehaviour
{
    [SerializeField]
    private Material[] m_materials;
    private Renderer main;

    private void Awake()
    {
        main = GetComponent<Renderer>();
        main.material = m_materials[Random.Range(0,m_materials.Length)];
    }

    public void ChangeColor(Material newMat)
    {
        main.material = newMat;
    }
}
