﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeManager : MonoBehaviour
{
    public static TimeManager Instance { get; private set; }

    [SerializeField]
    private float m_roundTimeLimit;

    [SerializeField]
    private float m_roundTime;

    [SerializeField]
    private float m_extraTime;

    [SerializeField]
    private float m_minExtraTime;

    [SerializeField]
    private float m_timeReduction;

    public int CurrentScore { get; set; }
    public float Timer { get; set; }
    public bool isLimitedTime { get; set; }
    public bool timeIsOut { get; set; }
    public float GameDuration { get; private set; }

    private float currentExtraTime;

    private bool isNoTimer = false;

    private void Awake()
    {
        if (Instance != null)
            Destroy(this);
        else
            Instance = this;
        Timer = m_roundTime;
        isLimitedTime = false;
        timeIsOut = false;
        currentExtraTime = m_extraTime;
    }

    private void Start()
    {
        FieldController.Instance.OnInactiveAdd += AddTime;
        ScoreManager.Instance.OnScoreTimeUpdate += ChangeTimeLimits;
    }

    private void OnDestroy()
    {
        FieldController.Instance.OnInactiveAdd -= AddTime;
        ScoreManager.Instance.OnScoreTimeUpdate -= ChangeTimeLimits;
    }

    // Update is called once per frame
    void Update()
    {
        if (Time.timeScale != 0 && !timeIsOut)
            GameDuration += Time.deltaTime;

        if (timeIsOut)
            return;

        if (isNoTimer)
            return;

        if (!isLimitedTime)
            return;

        if (Timer <= 0 && !FieldController.Instance.GameIsOver)
        {
            if(GameMenuUIManager.Instance)
            {
                GameMenuUIManager.Instance.OpenTimeOutMenu();
                FieldController.Instance.canPush = false;
                timeIsOut = true;
            }
        }
        else
        {
            Timer -= Time.deltaTime;
            GameMenuUIManager.Instance.UpdateTimer(Timer.ToString("#.0"));
        }
    }

    public void SetLimitedTime()
    {
        isNoTimer = !isNoTimer;
        if (isNoTimer == true)
            GameMenuUIManager.Instance.UpdateTimer("");
    }

    private void AddTime()
    {
        ChangeTime(m_extraTime);
        GameMenuUIManager.Instance.AnimateTimer(true);
    }

    public void ReduceTime()
    {
        ChangeTime(-m_timeReduction);
        GameMenuUIManager.Instance.AnimateTimer(false);
    }

    public void ChangeTime(float time)
    {
        if (!isNoTimer)
        {
            Timer += time;
            Timer = Mathf.Clamp(Timer, 0, m_roundTimeLimit);
            GameMenuUIManager.Instance.UpdateTimer(Timer.ToString("#.0"));
        }
    }

    private void ChangeTimeLimits()
    {
        currentExtraTime--;
        currentExtraTime = Mathf.Clamp(currentExtraTime, m_minExtraTime, m_extraTime);
    }
}
