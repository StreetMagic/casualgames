﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class FakeFieldController : FieldController
{
    public static FakeFieldController FakeInstance { get; private set; }
    private IEnumerator myCoroutine;
    private FieldObjectsData currentData;

    private SkinsDatabase data;

    void Awake()
    {
        if (FakeInstance != null)
            Destroy(this);
        else
            FakeInstance = this;
    }

    override protected void Start()
    {
        mainCam = Camera.main;
        ApplySkin();
        SkinManager.Instance.OnSkinChanged += ApplySkin;
    }

    private void OnDestroy()
    {
        SkinManager.Instance.OnSkinChanged -= ApplySkin;
    }

    private void ApplySkin()
    {
        data = Resources.Load<SkinsDatabase>("Databases/FieldObjectsData");
        currentData = data.GetObjectByName(SkinManager.Instance.GetSkinName());
        m_fieldObject = currentData.FieldObjects;
    }

    public override void Push(InputData data)
    {
        if (!canPush)
            return;

        Vector3 pos = mainCam.ScreenToWorldPoint(data.StartPositon);
        if (pos.x > 0)
            PushObjects(false);
        else
            PushObjects(true);
    }

    public override void AddToActiveList(FieldObject obj)
    {
        if (!ActiveObjects.Contains(obj))
        {
            ActiveObjects.Add(obj);
        }
    }

    public override void RemoveFromList(FieldObject obj)
    {
        if (ActiveObjects.Contains(obj))
        {
            ActiveObjects.Remove(obj);
        }
    }

    public override void AddToInactiveList(FieldObject obj, bool givePoints)
    {
        if (!InactiveObjects.Contains(obj))
        {
            InactiveObjects.Add(obj);
            if (ActiveObjects.Count <= 3)
            {
                int i; int nm;
                i = UnityEngine.Random.Range(0, m_fieldObject.Length - 2);
                nm = UnityEngine.Random.Range(0, m_fieldObject.Length - 2);
                SpawnFigure(i, nm);
            }
        }
    }

    public override void DestroyColumn()
    {
        StartCoroutine(DestroyColumnCoroutine());
    }

    private IEnumerator DestroyColumnCoroutine()
    {
        canPush = false;

        for (int i = InactiveObjects.Count - 1; i >= 0; i--)
        {
            InactiveObjects[i].DestroyFieldObject();
            yield return new WaitForSeconds(0.15f);
            Destroy(InactiveObjects[i].gameObject);
            InactiveObjects.Remove(InactiveObjects[i]);
        }

        canPush = true;
    }

    private void PushObjects(bool pushLeftGroup)
    {
        if (pushLeftGroup)
        {
            foreach (var item in ActiveObjects)
            {
                if (item.transform.position.x < 0)
                    item.AddForce(Vector2.right);
            }
        }
        else
        {
            foreach (var item in ActiveObjects)
            {
                if (item.transform.position.x > 0)
                    item.AddForce(Vector2.left);
            }
        }
    }

    public override void SpawnFigure(int number1, int number2)
    {
        Instantiate(m_fieldObject[number1], new Vector3(-16f, 20f, 12f), m_fieldObject[number1].transform.rotation);
        Instantiate(m_fieldObject[number2], new Vector3(16f, 20f, 12f), m_fieldObject[number2].transform.rotation);
    }

    public void RestartGame()
    {
        DestroyActiveObjects();
        DestroyInactiveObjects();
    }

    override public void DestroyInactiveObjects()
    {
        foreach (var item in InactiveObjects)
        {
            Destroy(item.gameObject);
        }
        InactiveObjects.Clear();
    }

    public void DestroyAllActiveObjects()
    {
        for (int i = ActiveObjects.Count - 1; i >= 0; i--)
        {
            Destroy(ActiveObjects[i].gameObject);
        }
        ActiveObjects.Clear();
    }
}