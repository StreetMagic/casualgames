﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuBackgroundSwitcher : MonoBehaviour
{
    [SerializeField]
    private GameObject m_lemingsBack;

    [SerializeField]
    private GameObject m_mathBack;

    [SerializeField]
    private GameObject m_colorsBack;


    // Use this for initialization
    void Start()
    {
        BackChange();
        SkinManager.Instance.OnSkinChanged += BackChange;
    }

    private void OnDestroy()
    {
        SkinManager.Instance.OnSkinChanged -= BackChange;
    }

    private void BackChange()
    {
        string name = SkinManager.Instance.GetSkinName();
        switch (name)
        {
            case "Lemings":
                m_lemingsBack.SetActive(true);
                m_mathBack.SetActive(false);
                m_colorsBack.SetActive(false);
                break;
            case "Math":
                m_lemingsBack.SetActive(false);
                m_mathBack.SetActive(true);
                m_colorsBack.SetActive(false);
                break;
            case "Colors":
                m_lemingsBack.SetActive(false);
                m_mathBack.SetActive(false);
                m_colorsBack.SetActive(true);
                break;
            default:
                break;
        }
    }
}
