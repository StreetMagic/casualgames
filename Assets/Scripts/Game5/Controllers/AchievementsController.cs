﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;


public enum AchievementAim { score, towers, time, win }
public class AchievementsController : MonoBehaviour
{
    public static AchievementsController Instance { get; private set; }

    [SerializeField]
    private Transform m_achievementsRoot;

    private Dictionary<AchievementAim, int> AchievementProgress = new Dictionary<AchievementAim, int>();
    private List<AchiveItem> CompletedAcheivements = new List<AchiveItem>();
    private AchiveItem currentAchievement;
    private SavedAchievementData currentData;
    private AchievementsDatabase database;
    private ShopItemsData shopData;

    private int score;
    private int towersCount;
    private float time;
    private int winCount;

    private string path;

    void Awake()
    {
        if (Instance != null)
            Destroy(this);
        else
            Instance = this;
    }

    // Use this for initialization
    void Start()
    {
        database = Resources.Load<AchievementsDatabase>("Databases/AchievementsDatabase");
        shopData = Resources.Load<ShopItemsData>("Databases/ShopItemsDatabase");
        LoadProgress();
        FieldController.Instance.OnGameOverAction += UpdateResults;
    }

    private void OnDestroy()
    {
        FieldController.Instance.OnGameOverAction -= UpdateResults;
    }

    private void UpdateResults()
    {
        score = ScoreManager.Instance.CurrentScore;
        towersCount = ScoreManager.Instance.CurrentTowers + GetData(AchievementAim.towers);
        time = TimeManager.Instance.GameDuration / 60 + GetData(AchievementAim.time);
        winCount = Mathf.Clamp(GetData(AchievementAim.win), 0, 90000) + 1;

        SetData(AchievementAim.score, GetData(AchievementAim.score) + score);
        SetData(AchievementAim.time, (int)time);
        SetData(AchievementAim.towers, towersCount);
        SetData(AchievementAim.win, winCount);

        CheckForAchievements();
    }

    private void CheckForAchievements()
    {
        foreach (var item in database.AchievementDatabase)
        {
            CheckAchievement(item);
        }
        SaveData();
        SaveSOData();
        ShowAchievement();
    }

    private void CheckAchievement(AchiveItem achievement)
    {
        if (achievement.IsAchieved)
            return;

        bool achieved = false;

        switch (achievement.Aim)
        {
            case AchievementAim.score:
                if (score >= achievement.AimAmount)
                    achieved = true;
                break;
            case AchievementAim.towers:
                if (towersCount >= achievement.AimAmount)
                    achieved = true;
                break;
            case AchievementAim.time:
                if (time >= achievement.AimAmount)
                    achieved = true;
                break;
            case AchievementAim.win:
                if (winCount >= achievement.AimAmount)
                    achieved = true;
                break;
            default:
                break;
        }

        if (!achieved)
            return;

        CompletedAcheivements.Add(achievement);
        achievement.IsAchieved = true;
        AddRewards(achievement);
        database.SetObjectByName(achievement.Name, achievement);
    }

    private void ShowAchievement()
    {
        if (CompletedAcheivements.Count > 0)
        {
            currentAchievement = CompletedAcheivements[0];
            RewardAchievementPanel.NewRewardPanel(currentAchievement, m_achievementsRoot);
        }
    }

    public void CloseAchievement()
    {
        CompletedAcheivements.RemoveAt(0);
        ShowAchievement();
    }

    private void AddRewards(AchiveItem achievement)
    {
        if (achievement.RewardBonuses > 0)
            CurrencyManager.Instance.AddActiveBonuses(achievement.RewardBonuses);

        if (achievement.RewardDiamonds > 0)
            CurrencyManager.Instance.AddCoins(achievement.RewardDiamonds);

        if (string.IsNullOrEmpty(achievement.RewardSkinName))
        {
            shopData.SetObjectByName(achievement.RewardSkinName);
        }
    }

    public int GetData(AchievementAim key)
    {
        int value = -1;
        if (AchievementProgress.ContainsKey(key))
        {
            value = AchievementProgress[key];
        }
        return value;
    }

    public void SetData(AchievementAim key, int value)
    {
        if (AchievementProgress.ContainsKey(key))
        {
            AchievementProgress[key] = value;
        }
        else
        {
            AchievementProgress.Add(key, value);
        }
        SaveData();
    }

    private void SaveData()
    {
        List<SavedData> newDataItems = new List<SavedData>();

        foreach (var save in AchievementProgress)
        {
            SavedData savedData = new SavedData();
            savedData.key = save.Key;
            savedData.value = save.Value;
            newDataItems.Add(savedData);
        }

        currentData = new SavedAchievementData();
        currentData.data = new SavedData[newDataItems.Count];
        for (int i = 0; i < newDataItems.Count; i++)
        {
            currentData.data[i] = newDataItems[i];
        }

        string dataAsText = JsonUtility.ToJson(currentData);
        File.WriteAllText(path, dataAsText);
    }

    [Header("File Name")]
    [SerializeField]
    private string persisterName;
    [Header("Scriptable Objects")]
    [SerializeField]
    private ScriptableObject objectsToPersist;

    protected void SaveSOData()
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + string.Format("/{0}.pso", persisterName));
        var json = JsonUtility.ToJson(objectsToPersist);
        bf.Serialize(file, json);
        file.Close();
    }

    private void LoadProgress()
    {
        path = Application.persistentDataPath + "/AchievementProgress.json";

        if (File.Exists(path))
        {
            currentData = JsonUtility.FromJson<SavedAchievementData>(File.ReadAllText(path));
        }
        else
        {
            TextAsset asset = Resources.Load<TextAsset>("AchievementProgress");
            currentData = JsonUtility.FromJson<SavedAchievementData>(asset.ToString());
        }

        AchievementProgress.Clear();
        if (currentData != null)
        {
            for (int i = 0; i < currentData.data.Length; i++)
            {
                AchievementProgress.Add(currentData.data[i].key, currentData.data[i].value);
            }
        }
    }
}

[Serializable]
public class SavedAchievementData
{
    public SavedData[] data;
}

[Serializable]
public class SavedData
{
    public AchievementAim key;
    public int value;
}