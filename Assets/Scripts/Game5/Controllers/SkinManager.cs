﻿using System;
using UnityEngine;

public class SkinManager : MonoBehaviour
{
    public static SkinManager Instance { get; private set; }

    [SerializeField]
    private LevelButton m_levelLoader;

    [SerializeField]
    private MenuSaveChecker m_saves;

    private string SkinName;

    public Action OnSkinChanged;

    private void Awake()
    {
        if (Instance != null)
            Destroy(this);
        else
            Instance = this;
        //PlayerPrefs.DeleteAll();
        SkinName = PlayerPrefs.GetString("ActiveSkinName");
        if (string.IsNullOrEmpty(SkinName))
            SkinName = "Lemings";
    }

    public string GetSkinName()
    {
        return SkinName;
    }

    public void SetSkinName(string name)
    {
        SkinName = name;
        PlayerPrefs.SetString("ActiveSkinName", name);
        m_saves.ResetSave();
        if (OnSkinChanged != null)
            OnSkinChanged();
    }

    public void SetSkinAndStart(string name)
    {
        if (SkinName != name)
        {
            m_saves.ResetSave();
        }
        SkinName = name;
        PlayerPrefs.SetString("ActiveSkinName", name);
        m_levelLoader.ResetScene(1);
    }
}
