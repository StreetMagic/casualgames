﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreManager : MonoBehaviour
{
    public static ScoreManager Instance { get; private set; }

    [SerializeField]
    private int m_scoreTimeEdge;

    [SerializeField]
    private int m_scoreFigureTypeEdge;

    [SerializeField]
    private int m_scoreBigSizeEdge;

    [SerializeField]
    private int m_scoreTriangleEdge;

    public int CurrentScore { get; set; }
    public int CurrentTowers { get; set; }

    public Action OnScoreTimeUpdate;
    public Action OnScoreTypeUpdate;
    public Action OnScoreBigSize;
    public Action OnScoreTriangle;

    private int cappedTimeScore;
    private int cappedTypeScore;
    private int cappedBigSizeScore;
    private int cappedTriangleScore;

    private void Awake()
    {
        if (Instance != null)
            Destroy(this);
        else
            Instance = this;
    }

    public void AddScore(int points)
    {
        CurrentScore += points;
        cappedTimeScore += points;
        cappedTypeScore += points;
        cappedBigSizeScore += points;
        cappedTriangleScore += points;

        if (GameMenuUIManager.Instance)
            GameMenuUIManager.Instance.UpdateScore(CurrentScore);

        if (cappedTimeScore >= m_scoreTimeEdge)
        {
            if (OnScoreTimeUpdate != null)
                OnScoreTimeUpdate();
            cappedTimeScore = 0;
        }

        if (cappedTypeScore >= m_scoreFigureTypeEdge)
        {
            if (OnScoreTypeUpdate != null)
                OnScoreTypeUpdate();
            cappedTypeScore = 0;
        }

        if (cappedBigSizeScore >= m_scoreBigSizeEdge)
        {
            if (OnScoreBigSize != null)
                OnScoreBigSize();
            cappedBigSizeScore = 0;
        }
        else if (cappedTriangleScore >= m_scoreTriangleEdge)
        {
            if (OnScoreTriangle != null)
                OnScoreTriangle();
            cappedTriangleScore = 0;
        }
    }

    public void AddTower()
    {
        CurrentTowers++;
    }
}
