﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CurrencyManager : MonoBehaviour
{
    public static CurrencyManager Instance { get; private set; }

    public int BonusDiamonds { get; set; }
    public int CurrentBonusCount { get; private set; }
    private int currentCoins;
    private int roundCoins;

    public Action OnCurrencyUpdateAction;
    public Action OnBonusSpentAction;

    private void Awake()
    {
        if (Instance)
            Destroy(this);
        else
            Instance = this;

        currentCoins = PlayerPrefs.GetInt("CoinsBank", 0);
        CurrentBonusCount = PlayerPrefs.GetInt("ActiveBonusCount", 4);
    }

    private void Start()
    {
        FieldController.Instance.OnGameOverAction += SetCurrency;
    }

    private void OnDestroy()
    {
        FieldController.Instance.OnGameOverAction -= SetCurrency;
    }

    private void SetCurrency()
    {
        roundCoins = ScoreManager.Instance.CurrentScore;
        currentCoins += roundCoins;
        PlayerPrefs.SetInt("CoinsBank", currentCoins);
    }

    public void AddCoins(int amount)
    {
        currentCoins += amount;
        PlayerPrefs.SetInt("CoinsBank", currentCoins);

        if (OnCurrencyUpdateAction != null)
            OnCurrencyUpdateAction();
    }

    public void SpendCoins(int amount)
    {
        if (currentCoins < amount)
            return;

        currentCoins -= amount;
        PlayerPrefs.SetInt("CoinsBank", currentCoins);

        if (OnCurrencyUpdateAction != null)
            OnCurrencyUpdateAction();
    }

    public bool PriceCheck(int cost)
    {
        if (cost <= currentCoins)
            return true;
        else
            return false;
    }

    public int GetRoundCurrency()
    {
        return roundCoins;
    }

    public int GetCurrentCurrency()
    {
        return currentCoins;
    }

    public int CountBonusCurrency()
    {
        BonusDiamonds = (int)(roundCoins * 0.2f);
        return BonusDiamonds;
    }

    public void AddActiveBonuses(int amount)
    {
        CurrentBonusCount += amount;
        CurrentBonusCount = Mathf.Clamp(CurrentBonusCount, 0, 99999);
        PlayerPrefs.SetInt("ActiveBonusCount", CurrentBonusCount);
    }
}
