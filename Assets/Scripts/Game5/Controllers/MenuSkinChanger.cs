﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuSkinChanger : MonoBehaviour
{
    public static MenuSkinChanger Instance { get; private set; }

    [SerializeField]
    private FakeFieldController controller;

    [SerializeField]
    private FakePlayer player;

    private void Awake()
    {
        if (Instance != null)
            Destroy(this);
        else
            Instance = this;
    }

    private void Start()
    {
        SkinManager.Instance.OnSkinChanged += ChangeSkin;
    }

    private void OnDestroy()
    {
        SkinManager.Instance.OnSkinChanged -= ChangeSkin;
    }

    public void ChangeSkin()
    {
        controller.RestartGame();
        player.RestartGame();
    }
    
}
