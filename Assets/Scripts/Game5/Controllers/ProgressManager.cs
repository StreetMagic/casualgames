﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ProgressManager : MonoBehaviour
{
    public static ProgressManager Instance { get; private set; }

    private SaveData saveData;
    private string path;

    public Action OnLoadDataAction;

    void Awake()
    {
        if (Instance != null)
            Destroy(this);
        else
            Instance = this;

        path = Application.persistentDataPath + "/SavedProgress.json";
        //path = "Assets/Resources/SavedProgress.json";

        if (File.Exists(path))
        {
            saveData = JsonUtility.FromJson<SaveData>(File.ReadAllText(path));
        }
        else
        {
            TextAsset asset = Resources.Load<TextAsset>("SavedProgress");
            saveData = JsonUtility.FromJson<SaveData>(asset.ToString());
        }
    }
    // Use this for initialization
    IEnumerator Start()
    {
        while (PlayerPrefs.GetInt("TutorialComplete", 0) == 0)
        {
            yield return null;
        }

        if (saveData.isSaved)
        {
            LoadData();
            if (OnLoadDataAction != null)
                OnLoadDataAction();
        }
        else
        {
            SpawnManager.Instance.StartNewGame();
        }

        FieldController.Instance.OnGameOverAction += RemoveSave;
    }

    private void OnDestroy()
    {
        FieldController.Instance.OnGameOverAction -= RemoveSave;
    }

    private void RemoveSave()
    {
        saveData = new SaveData();
        saveData.isSaved = false;
        string dataAsText = JsonUtility.ToJson(saveData);
        File.WriteAllText(path, dataAsText);
    }

    private void OnApplicationFocus(bool focus)
    {
        if (focus)
            return;

        if (Application.isEditor)
            return;

        if (SceneManager.GetActiveScene().name != "Game5")
            return;

        if (FieldController.Instance.GameIsOver)
            return;

        if (ScoreManager.Instance.CurrentScore <= 0)
            return;
               
        SaveData();
    }

    private void LoadData()
    {
        TimeManager.Instance.Timer = saveData.time;
        GameMenuUIManager.Instance.UpdateTimer(saveData.time.ToString("#.0"));
        ScoreManager.Instance.CurrentScore = saveData.score;
        GameMenuUIManager.Instance.UpdateScore(saveData.score);
        SpawnManager.Instance.currentTypesCount = saveData.openedFigureTypes;

        for (int i = 0; i < saveData.inactiveObjects.Length; i++)
        {
            FieldObjectSaveData item = saveData.inactiveObjects[i];
            GameObject obj = Resources.Load<GameObject>(item.marker);
            obj = Instantiate(obj, item.Position, item.Rotation);
            obj.GetComponent<FieldObject>().isActive = false;
        }

        for (int i = 0; i < saveData.activeObjects.Length; i++)
        {
            FieldObjectSaveData item = saveData.activeObjects[i];
            GameObject obj = Resources.Load<GameObject>(item.marker);
            obj = Instantiate(obj, item.Position, item.Rotation);
            obj.GetComponent<FieldObject>().isActive = true;
        }

        Invoke("EnableTap", 1f);
    }

    private void SaveData()
    {
        saveData = new SaveData();
        saveData.isSaved = true;
        saveData.score = ScoreManager.Instance.CurrentScore;
        saveData.time = TimeManager.Instance.Timer;
        saveData.openedFigureTypes = SpawnManager.Instance.currentTypesCount;

        List<FieldObject> activeObjects = FieldController.Instance.GetActiveObjects();
        saveData.activeObjects = new FieldObjectSaveData[activeObjects.Count];
        for (int i = 0; i < activeObjects.Count; i++)
        {
            saveData.activeObjects[i] = new FieldObjectSaveData();
            saveData.activeObjects[i].marker = activeObjects[i].GetComponent<FieldObjectMarker>().Marker;
            saveData.activeObjects[i].Position = activeObjects[i].transform.position;
            saveData.activeObjects[i].Rotation = activeObjects[i].transform.rotation;
        }

        List<FieldObject> inactiveObjects = FieldController.Instance.GetInactiveObjects();
        saveData.inactiveObjects = new FieldObjectSaveData[inactiveObjects.Count];
        for (int i = 0; i < inactiveObjects.Count; i++)
        {
            saveData.inactiveObjects[i] = new FieldObjectSaveData();
            saveData.inactiveObjects[i].marker = inactiveObjects[i].GetComponent<FieldObjectMarker>().Marker;
            saveData.inactiveObjects[i].Position = inactiveObjects[i].transform.position;
            saveData.inactiveObjects[i].Rotation = inactiveObjects[i].transform.rotation;
        }

        string dataAsText = JsonUtility.ToJson(saveData);
        File.WriteAllText(path, dataAsText);
    }

    [ContextMenu("SaveProgress")]
    public void SaveNewData()
    {
        SaveData();
    }

    private void EnableTap()
    {
        FieldController.Instance.canPush = true;
        TimeManager.Instance.isLimitedTime = true;
        SpawnManager.Instance.CurrentGameState = GameStates.Started;
    }

    public void ResetSave()
    {
        saveData = new SaveData();
        saveData.isSaved = false;
        string dataAsText = JsonUtility.ToJson(saveData);
        File.WriteAllText(path, dataAsText);
    }
}

[Serializable]
public class SaveData
{
    public bool isSaved;
    public int score;
    public float time;
    public int openedFigureTypes;
    public FieldObjectSaveData[] activeObjects;
    public FieldObjectSaveData[] inactiveObjects;
}

[Serializable]
public class FieldObjectSaveData
{
    public string marker;
    public Vector3 Position;
    public Quaternion Rotation;
}