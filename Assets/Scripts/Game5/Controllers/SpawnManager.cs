﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum GameStates { notStarted, Started, Paused, isOver }
public class SpawnManager : MonoBehaviour
{
    public static SpawnManager Instance { get; private set; }

    [SerializeField]
    private int m_startTypesCount;

    [SerializeField]
    private int m_minActiveObjects;

    [SerializeField]
    private bool m_canSpawnBig;


    public GameStates CurrentGameState { get; set; }
    public int currentTypesCount { get; set; }
    private int totalObjectsCount;

    private int TotalObjectsCount
    {
        get
        {
            if (totalObjectsCount <= 0)
            {
                totalObjectsCount = FieldController.Instance.GetFieldObjectCount();
            }
            return totalObjectsCount;
        }
        set
        {
            totalObjectsCount = value;
        }
    }

    private bool spawnBigOne = false;

    private void Awake()
    {
        if (Instance != null)
            Destroy(this);
        else
            Instance = this;
        CurrentGameState = GameStates.notStarted;
        currentTypesCount = m_startTypesCount;
    }

    private void Start()
    {
        FieldController.Instance.OnInactiveAdd += SpawnCheck;
        ScoreManager.Instance.OnScoreTypeUpdate += TypeCountChange;
        ScoreManager.Instance.OnScoreBigSize += SpawnBigSize;
        ScoreManager.Instance.OnScoreTriangle += SpawnTriangle;
    }


    private void OnDestroy()
    {
        FieldController.Instance.OnInactiveAdd -= SpawnCheck;
        ScoreManager.Instance.OnScoreTypeUpdate -= TypeCountChange;
        ScoreManager.Instance.OnScoreBigSize -= SpawnBigSize;
        ScoreManager.Instance.OnScoreTriangle -= SpawnTriangle;
    }

    private void SpawnTriangle()
    {
        int n = TotalObjectsCount;
        int a = UnityEngine.Random.Range(0, 1);
        if (a == 0)
            FieldController.Instance.SpawnFigure(-UnityEngine.Random.Range(n - 3, n), UnityEngine.Random.Range(n - 3, n));
        else
            FieldController.Instance.SpawnFigure(UnityEngine.Random.Range(n - 3, n), -UnityEngine.Random.Range(n - 3, n));
    }

    private void SpawnBigSize()
    {
        spawnBigOne = true;
    }

    private void TypeCountChange()
    {
        if (currentTypesCount >= TotalObjectsCount - 3)
            return;

        currentTypesCount++;
        //currentTypesCount = Mathf.Clamp(currentTypesCount, m_startTypesCount, totalObjectsCount - 3);
    }

    private void SpawnCheck()
    {
        if (CurrentGameState != GameStates.Started)
            return;

        if (FieldController.Instance.GetActiveObjects().Count <= m_minActiveObjects)
        {
            int i = currentTypesCount;
            if (spawnBigOne && m_canSpawnBig)
            {
                int n = TotalObjectsCount;
                int a = UnityEngine.Random.Range(0, 1);
                if (a == 0)
                    FieldController.Instance.SpawnFigure(UnityEngine.Random.Range(n - 6, n - 3), UnityEngine.Random.Range(0, i));
                else
                    FieldController.Instance.SpawnFigure(UnityEngine.Random.Range(0, i), UnityEngine.Random.Range(n - 6, n - 3));
                spawnBigOne = false;
            }
            else
                FieldController.Instance.SpawnFigure(UnityEngine.Random.Range(0, i), UnityEngine.Random.Range(0, i));
        }
    }

    public void StartNewGame()
    {
        StartCoroutine(StartGameCoroutine());
    }

    private IEnumerator StartGameCoroutine()
    {
        int i = currentTypesCount;
        yield return new WaitForSeconds(0.2f);
        FieldController.Instance.SpawnFigure(UnityEngine.Random.Range(0, i), UnityEngine.Random.Range(0, i));
        yield return new WaitForSeconds(1f);
        FieldController.Instance.SpawnFigure(UnityEngine.Random.Range(0, i), UnityEngine.Random.Range(0, i));
        yield return new WaitForSeconds(1f);
        FieldController.Instance.SpawnFigure(UnityEngine.Random.Range(0, i), UnityEngine.Random.Range(0, i));
        yield return new WaitForSeconds(1f);
        FieldController.Instance.canPush = true;
        TimeManager.Instance.isLimitedTime = true;
        CurrentGameState = GameStates.Started;
        TotalObjectsCount = FieldController.Instance.GetFieldObjectCount();
    }

    private void ChangeGameState(GameStates newState)
    {
        CurrentGameState = newState;
    }


}
