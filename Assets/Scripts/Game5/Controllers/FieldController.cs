﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class FieldController : MonoBehaviour
{
    public static FieldController Instance { get; private set; }

    [SerializeField]
    protected GameObject[] m_fieldObject;

    [SerializeField]
    private Color m_ScrollUpColor;

    public Action OnGameOverAction;
    public Action OnInactiveAdd;
    public Action OnActiveChange;
    public Action OnDestroyColumn;

    private bool gameIsOver = false;
    public bool GameIsOver
    {
        get { return gameIsOver; }
        set
        {
            gameIsOver = value;
            if (gameIsOver == true)
            {
                GameMenuUIManager.Instance.UpdateTimer("GAME OVER!");
            }
        }
    }

    [HideInInspector]
    public bool canPush = false;
    [HideInInspector]
    public bool fadeOnTaps = false;
    [HideInInspector]
    public bool canDestroyObjects = false;

    protected Camera mainCam;
    protected List<FieldObject> ActiveObjects = new List<FieldObject>();
    protected List<FieldObject> InactiveObjects = new List<FieldObject>();
    private int ControllType = 1;
    private Animator myAnimator;

    private FieldObjectsData myData;

    private IEnumerator myCoroutine;

    private void Awake()
    {
        if (Instance != null)
            Destroy(this);
        else
            Instance = this;
    }

    virtual protected void Start()
    {
        myAnimator = GetComponent<Animator>();
        InputManager.Instance.TapAction += Push;
        mainCam = Camera.main;


        SkinsDatabase data = Resources.Load<SkinsDatabase>("Databases/FieldObjectsData");
        myData = data.GetObjectByName(SkinManager.Instance.GetSkinName());
        m_fieldObject = myData.FieldObjects;
        Instantiate(myData.Background);
    }

    private void OnDestroy()
    {
        InputManager.Instance.TapAction -= Push;
    }

    virtual public void Push(InputData data)
    {
        if (!canPush)
            return;

        if (GameIsOver)
            return;

        Vector3 pos = mainCam.ScreenToWorldPoint(data.StartPositon);
        if (pos.y > 40f || pos.y < -40f)
            return;

        if (myAnimator && fadeOnTaps)
        {
            if (pos.x > 0)
            {
                if (myAnimator.GetCurrentAnimatorStateInfo(0).IsName("BaseState"))
                    myAnimator.SetTrigger("FadeRight");
            }
            else
            {
                if (myAnimator.GetCurrentAnimatorStateInfo(0).IsName("BaseState"))
                    myAnimator.SetTrigger("FadeLeft");
            }
        }


        switch (ControllType)
        {
            case 1:
                if (pos.x > 0)
                    PushObjects(false);
                else
                    PushObjects(true);
                break;
            case 2:
                if (pos.x > 0)
                    PushAllObjects(true);
                else
                    PushAllObjects(false);
                break;
            case 3:
                if (pos.x > 0)
                    InversePushObjects(true);
                else
                    InversePushObjects(false);
                break;
            default:
                break;
        }
    }

    virtual public void AddToActiveList(FieldObject obj)
    {
        if (!ActiveObjects.Contains(obj))
        {
            ActiveObjects.Add(obj);
        }
    }

    virtual public void RemoveFromList(FieldObject obj)
    {
        if (ActiveObjects.Contains(obj))
        {
            ActiveObjects.Remove(obj);
        }
    }

    virtual public void AddToInactiveList(FieldObject obj, bool givePoints)
    {
        if (!InactiveObjects.Contains(obj))
        {
            InactiveObjects.Add(obj);
            if (OnInactiveAdd != null)
                OnInactiveAdd();
        }
    }

    virtual public void DestroyColumn()
    {
        StartCoroutine(DestroyColumnCoroutine(true));
    }

    [ContextMenu("DestroyColumn")]
    public void DestroyAll()
    {
        DestroyColumn();
    }

    private IEnumerator DestroyColumnCoroutine(bool getScore)
    {
        int bonusScore = InactiveObjects.Count;
        TimeManager.Instance.isLimitedTime = false;
        canPush = false;

        foreach (var item in InactiveObjects)
        {
            item.gameObject.layer = 10;
        }

        if (OnDestroyColumn != null)
            OnDestroyColumn();

        //if (TrailRendererDrawer.Instance)
        //    TrailRendererDrawer.Instance.SetPositions(InactiveObjects);
        //else
        //    canDestroyObjects = true;

        InactiveObjects = InactiveObjects.OrderBy(t => t.transform.position.y).ToList();

        while (!canDestroyObjects)
        {
            yield return null;
        }

        for (int i = InactiveObjects.Count - 1; i >= 0; i--)
        {
            InactiveObjects[i].DestroyFieldObject();
            yield return new WaitForSeconds(0.15f);
            Destroy(InactiveObjects[i].gameObject);
            InactiveObjects.Remove(InactiveObjects[i]);
        }

        if (getScore)
        {
            yield return new WaitForSeconds(0.15f);
            ScoreManager.Instance.AddScore(bonusScore);
            ScoreManager.Instance.AddTower();
            //var newText = ScrollUpText.GetScrollUpText("+" + bonusScore.ToString(), new Vector3(0, -10, 0), m_ScrollUpColor);
            //if (newText)
            //    Destroy(newText.gameObject, 2f);
        }

        canPush = true;
        TimeManager.Instance.isLimitedTime = true;
        canDestroyObjects = false;
    }

    private void PushObjects(bool pushLeftGroup)
    {
        if (pushLeftGroup)
        {
            foreach (var item in ActiveObjects)
            {
                if (item.transform.position.x < 0)
                    item.AddForce(Vector2.right);
            }
        }
        else
        {
            foreach (var item in ActiveObjects)
            {
                if (item.transform.position.x > 0)
                    item.AddForce(Vector2.left);
            }
        }
    }

    private void InversePushObjects(bool pushLeftGroup)
    {
        if (pushLeftGroup)
        {
            foreach (var item in ActiveObjects)
            {
                if (item.transform.position.x < 0)
                    item.AddForce(Vector2.right);
            }
        }
        else
        {
            foreach (var item in ActiveObjects)
            {
                if (item.transform.position.x > 0)
                    item.AddForce(Vector2.left);
            }
        }
    }

    private void PushAllObjects(bool pushToTheLeft)
    {
        if (!pushToTheLeft)
        {
            foreach (var item in ActiveObjects)
                item.AddForce(Vector2.right);
        }
        else
        {
            foreach (var item in ActiveObjects)
                item.AddForce(Vector2.left);
        }
    }

    virtual public void SpawnFigure(int number1, int number2)
    {
        //print(number1 + " AND " + number2);
        if (number1 >= 0)
            Instantiate(m_fieldObject[number1], new Vector3(-16f, 20f, 12f), m_fieldObject[number1].transform.rotation);
        if (number2 >= 0)
            Instantiate(m_fieldObject[number2], new Vector3(16f, 20f, 12f), m_fieldObject[number2].transform.rotation);
    }

    public void ShuffleFigures(bool toTheRight)
    {
        if (toTheRight)
        {
            foreach (var item in ActiveObjects)
            {
                if (item.transform.position.x < 0)
                {
                    Vector3 pos = item.transform.position;
                    pos.x = -pos.x;
                    item.transform.position = pos;
                }
            }
        }
        else
        {
            foreach (var item in ActiveObjects)
            {
                if (item.transform.position.x > 0)
                {
                    Vector3 pos = item.transform.position;
                    pos.x = -pos.x;
                    item.transform.position = pos;
                }
            }
        }
    }

    public void DestroyActiveObjects()
    {
        foreach (var item in ActiveObjects)
        {
            Destroy(item.gameObject);
        }
        ActiveObjects.Clear();
    }

    virtual public void DestroyInactiveObjects()
    {
        StartCoroutine(DestroyColumnCoroutine(false));
    }

    public void FadeBackgroundOnTaps()
    {
        fadeOnTaps = !fadeOnTaps;
    }

    public void SetControlType(int typeNumber)
    {
        ControllType = typeNumber;
    }

    public void ChangeObjects()
    {
        if (myCoroutine != null)
            return;

        myCoroutine = ChangeObjectsCoroutine();
        StartCoroutine(myCoroutine);
    }

    private IEnumerator ChangeObjectsCoroutine()
    {
        canPush = false;
        TimeManager.Instance.isLimitedTime = false;
        DestroyActiveObjects();
        yield return new WaitForSeconds(0.2f);
        SpawnFigure(UnityEngine.Random.Range(0, 1), UnityEngine.Random.Range(0, 1));
        yield return new WaitForSeconds(0.8f);
        SpawnFigure(UnityEngine.Random.Range(0, 3), UnityEngine.Random.Range(0, 3));
        yield return new WaitForSeconds(0.8f);
        SpawnFigure(UnityEngine.Random.Range(0, 6), UnityEngine.Random.Range(0, 6));
        yield return new WaitForSeconds(0.8f);
        canPush = true;
        TimeManager.Instance.isLimitedTime = true;
        myCoroutine = null;
    }

    public List<FieldObject> GetActiveObjects()
    {
        return ActiveObjects;
    }

    public List<FieldObject> GetInactiveObjects()
    {
        return InactiveObjects;
    }

    public int GetFieldObjectCount()
    {
        return m_fieldObject.Length;
    }

    public void DestroyTopItem()
    {
        List<FieldObject> newList = InactiveObjects.OrderByDescending(t => t.GetComponent<Collider2D>().bounds.max.y).ToList();
        newList[0].DestroyFieldObject();
        Destroy(newList[0].gameObject, 0.15f);
        InactiveObjects.Remove(newList[0]);
        //TimeManager.Instance.ReduceTime();
    }
}
