﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FakePlayer : MonoBehaviour
{
    [SerializeField]
    private float m_clickDelay;

    [SerializeField]
    private float m_pauseTime;

    Vector2 rightPartPos = new Vector2(5f, 5f);
    Vector2 lefttPartPos = new Vector2(-5f, 5f);

    private float clickTimer;
    private float pauseTimer;
    private int clickCount;
    private bool canClick;

    public bool isOnPause { get; set; }

    private IEnumerator myCoroutine;

    // Use this for initialization
    void Start()
    {
        rightPartPos = Camera.main.WorldToScreenPoint(rightPartPos);
        lefttPartPos = Camera.main.WorldToScreenPoint(lefttPartPos);
        StartCoroutine(FakePlayCoroutine());
    }

    private IEnumerator FakePlayCoroutine()
    {
        FakeFieldController.FakeInstance.SpawnFigure(Random.Range(0, 2), Random.Range(0, 2));
        FakeFieldController.FakeInstance.canPush = true;
        yield return new WaitForSeconds(0.5f);
        FakeFieldController.FakeInstance.SpawnFigure(Random.Range(0, 2), Random.Range(0, 2));
        clickTimer = 0;
        pauseTimer = m_pauseTime;
        while (true)
        {
            if (clickTimer > 0)
                clickTimer -= Time.deltaTime;
            else
            {
                if (canClick)
                {
                    var data = new InputData();
                    int random = Random.Range(0, 2);
                    data.StartPositon = random == 0 ? lefttPartPos : rightPartPos;
                    FakeFieldController.FakeInstance.Push(data);
                    clickCount++;

                    if (clickCount >= 10)
                    {
                        canClick = false;
                    }
                    else clickTimer = Random.Range(0.1f, m_clickDelay);
                }
            }
            if (!canClick)
            {
                if (pauseTimer > 0)
                    pauseTimer -= Time.deltaTime;
                else
                {
                    canClick = true;
                    pauseTimer = m_pauseTime;
                    clickCount = 0;
                }
            }
            yield return null;
        }
    }

    public void RestartGame()
    {
        StopAllCoroutines();
        if (myCoroutine != null)
            return;

        myCoroutine = FakePlayCoroutine();
        StartCoroutine(myCoroutine);
    }

    public void StopFakePlayer()
    {
        StopAllCoroutines();
        FakeFieldController.FakeInstance.RestartGame();
        myCoroutine = null;
    }
}
