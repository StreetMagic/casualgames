﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class TrailRendererDrawer : MonoBehaviour
{
    public static TrailRendererDrawer Instance { get; private set; }

    [SerializeField]
    private float m_drawSpeed;

    private LineRenderer myLine;
    public Vector2[] linePoints;

    void Awake()
    {
        if (Instance != null)
            Destroy(this);
        else
            Instance = this;
    }

    // Use this for initialization
    void Start()
    {
        myLine = GetComponent<LineRenderer>();
    }

    private IEnumerator LineDrawerCoroutine()
    {
        myLine.enabled = true;
        myLine.positionCount = 2;
        myLine.SetPosition(0, linePoints[0]);
        float t;
        int nm = 0;
        for (int i = 0; i < linePoints.Length-1; i++)
        {
            t = 0;
            if (i != 0)
                myLine.positionCount++;
            nm = i + 1;

            while (t < 1)
            {
                t += Time.deltaTime * m_drawSpeed;
                Vector2 destination = Vector2.Lerp(myLine.GetPosition(i), linePoints[nm], t);
                myLine.SetPosition(i + 1, destination);
                yield return null;
            }
        }
        FieldController.Instance.canDestroyObjects = true;
        yield return new WaitForSeconds(0.2f);
        myLine.positionCount = 0;
        myLine.enabled = false;

    }

    public void SetPositions(List<FieldObject> objects)
    {
        FieldObject[] currentObjects = SortList(objects);

        if (currentObjects.Length <= 0)
            return;

        linePoints = new Vector2[currentObjects.Length];
        for (int i = 0; i < currentObjects.Length; i++)
        {
            linePoints[i] = objects[i].transform.position;
        }
        StartCoroutine(LineDrawerCoroutine());
    }

    private FieldObject[] SortList(List<FieldObject> objects)
    {
        FieldObject[] currentObjects = new FieldObject[objects.Count];

        currentObjects = objects.OrderByDescending(t => t.transform.position.y).ToArray();

        return currentObjects;
    }
}