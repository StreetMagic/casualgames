﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class AchievementItem : MonoBehaviour
{
    [SerializeField]
    private Text m_nameText;
    [SerializeField]
    private Text m_descriptionText;

    [SerializeField]
    private Image m_icon;
    [SerializeField]
    private Sprite m_achievedSprite;

    [SerializeField]
    private Text m_diamondText;

    [SerializeField]
    private GameObject m_diamondIcon;

    [SerializeField]
    private Text m_bonusText;

    [SerializeField]
    private GameObject m_bonusIcon;

    [SerializeField]
    private Text m_skinText;

    private AchievementsDatabase data;
    private bool isAchieved = false;


    private void Start()
    {
        transform.localScale = new Vector3(1f, 1f, 1f);
        data = Resources.Load<AchievementsDatabase>("Databases/AchievementsDatabase");
    }

    public void SetDescription(string text)
    {
        m_descriptionText.text = text;
    }
    public void SetName(string name)
    {
        //List<char> separetedName = new List<char>();
        //for (int j = 0; j < name.Length; j++)
        //{
        //    if (Char.IsUpper(name[j]) && j != 0)
        //    {
        //        separetedName.Add(' ');
        //    }
        //    separetedName.Add(name[j]);
        //}
        //string newName = null;
        //foreach (var c in separetedName)
        //{
        //    newName += c;
        //}
        //newName = newName.ToUpper();
        //m_nameText.text = newName;

        m_nameText.text = name;
    }

    public void FillItem(AchiveItem item)
    {
        SetName(item.Name);
        m_descriptionText.text = item.Description;
        SetPanel(item);
        isAchieved = item.IsAchieved;
        if (isAchieved)
            m_icon.sprite = m_achievedSprite;
    }

    private void SetPanel(AchiveItem achievement)
    {
        if (achievement.RewardDiamonds <= 0)
        {
            Destroy(m_diamondText.gameObject);
            Destroy(m_diamondIcon.gameObject);
        }
        else
            m_diamondText.text = "+" + achievement.RewardDiamonds.ToString();
        if (achievement.RewardBonuses <= 0)
        {
            Destroy(m_bonusText.gameObject);
            Destroy(m_bonusIcon.gameObject);
        }
        else
            m_bonusText.text = "+" + achievement.RewardBonuses.ToString();
        if (string.IsNullOrEmpty(achievement.RewardSkinName))
        {
            Destroy(m_skinText.gameObject);
        }
        else
            m_skinText.text = "+" + achievement.RewardSkinName.ToString() + " skin";
    }
}

