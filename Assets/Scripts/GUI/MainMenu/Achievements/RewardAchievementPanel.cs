﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RewardAchievementPanel : MonoBehaviour
{
    [SerializeField]
    private Text m_nameText;

    [SerializeField]
    private Text m_discriptionText;

    [SerializeField]
    private Text m_diamondText;

    [SerializeField]
    private GameObject m_diamondIcon;

    [SerializeField]
    private Text m_bonusText;

    [SerializeField]
    private GameObject m_bonusIcon;

    [SerializeField]
    private Text m_skinText;

    public static RewardAchievementPanel NewRewardPanel(AchiveItem achievement, Transform parent)
    {
        RewardAchievementPanel panel = null;

        GameObject newPanel = Instantiate((Resources.Load("AchievementRewardPanel") as GameObject), parent);
        newPanel.transform.localScale = Vector3.one;
        panel = newPanel.GetComponent<RewardAchievementPanel>();
        panel.SetPanel(achievement);
        return panel;
    }

    public void ClosePanel()
    {
        AchievementsController.Instance.CloseAchievement();
        Destroy(gameObject);
    }

    public void SetPanel(AchiveItem achievement)
    {
        m_nameText.text = achievement.Name;
        m_discriptionText.text = achievement.Description;
        if (achievement.RewardDiamonds <= 0)
        {
            Destroy(m_diamondText.gameObject);
            Destroy(m_diamondIcon.gameObject);
        }
        else
            m_diamondText.text = "+" + achievement.RewardDiamonds.ToString();
        if (achievement.RewardBonuses <= 0)
        {
            Destroy(m_bonusText.gameObject);
            Destroy(m_bonusIcon.gameObject);
        }
        else
            m_bonusText.text = "+" + achievement.RewardBonuses.ToString();
        if (string.IsNullOrEmpty(achievement.RewardSkinName))
        {
            Destroy(m_skinText.gameObject);
        }
        else
            m_skinText.text = "+" + achievement.RewardSkinName.ToString() + " skin";

    }
}
