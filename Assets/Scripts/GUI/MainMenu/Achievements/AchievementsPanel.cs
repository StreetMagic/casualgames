﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AchievementsPanel : MonoBehaviour
{
    public static AchievementsPanel Instance { get; private set; }

    [SerializeField]
    private GameObject m_Content;

    [SerializeField]
    private GameObject m_AchievementItem;

    private AchiveItem[] AchieveList;
    private int diamondsCount;

    protected void Awake()
    {
        if (Instance != null)
            Destroy(this);
        else
            Instance = this;

        AchieveList = Resources.Load<AchievementsDatabase>("Databases/AchievementsDatabase").AchievementDatabase;
        for (int i = 0; i < AchieveList.Length; i++)
        {
            GameObject item = Instantiate(m_AchievementItem);
            item.transform.SetParent(m_Content.transform);
            var achievement = item.GetComponent<AchievementItem>();
            achievement.FillItem(AchieveList[i]);
        }
    }

    public void ClosePanel()
    {
        MainMenuController.Instance.CloseAchievementsPanel();
    }
}

