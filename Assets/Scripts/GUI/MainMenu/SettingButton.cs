﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SettingButton : MonoBehaviour
{

    [SerializeField]
    private Sprite m_ActiveSprite;

    [SerializeField]
    private Sprite m_InactiveSprite;

    private Button myButton;
    private Image myImage;
    private bool isActive;

    private void Awake()
    {
        myImage = GetComponent<Image>();
        myButton = GetComponent<Button>();
    }

    public void SetButton(bool isActive)
    {
        if(isActive)
        {
            if (myImage.sprite != m_ActiveSprite)
                myImage.sprite = m_ActiveSprite;
        }
        else
        {
            if (myImage.sprite != m_InactiveSprite)
                myImage.sprite = m_InactiveSprite;
        }
    }

    public void EnableSettingButton(bool enable)
    {
        myButton.enabled = enable;
    }
}
