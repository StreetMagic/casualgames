﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SettingsButtonController : MonoBehaviour
{
    [SerializeField]
    private SettingButton m_SoundButton;

    [SerializeField]
    private SettingButton m_MusicButton;

    private bool isMusicOn;
    private bool isSoundOn;
    private Animator myAnimator;

    // Use this for initialization
    void Start()
    {
        myAnimator = GetComponent<Animator>();
        isMusicOn = PlayerPrefs.GetInt("GameMusic", 0) > 0 ? true : false;
        m_MusicButton.SetButton(isMusicOn);
        isSoundOn = PlayerPrefs.GetInt("GameSound", 0) > 0 ? true : false;
        m_SoundButton.SetButton(isSoundOn);
    }

    public void TurnSound()
    {
        isSoundOn = !isSoundOn;
        PlayerPrefs.SetInt("GameSound", isSoundOn ? 1 : 0);
        m_SoundButton.SetButton(isSoundOn);
        //TODO
    }

    public void TurnMusic()
    {
        isMusicOn = !isMusicOn;
        PlayerPrefs.SetInt("GameMusic", isMusicOn ? 1 : 0);
        m_MusicButton.SetButton(isMusicOn);
        //TODO
    }

    public void Settings()
    {
        if (myAnimator.GetCurrentAnimatorStateInfo(0).IsName("SettingsOpen"))
        {
            m_SoundButton.EnableSettingButton(false);
            m_MusicButton.EnableSettingButton(false);
        }
        else
        {
            m_SoundButton.EnableSettingButton(true);
            m_MusicButton.EnableSettingButton(true);
        }
        myAnimator.SetTrigger("Activate");
    }
}
