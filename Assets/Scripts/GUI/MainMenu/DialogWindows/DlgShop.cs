﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DlgShop : MonoBehaviour
{
    public static DlgShop Instance { get; private set; }

    [SerializeField]
    private GameObject m_Content;
    [SerializeField]
    private GameObject m_ShopItem;

    [SerializeField]
    private GameObject m_ShopDoubleItem;

    [SerializeField]
    private Text m_moneyText;

    [SerializeField]
    private Text m_RewardText;

    [SerializeField]
    private Text m_RewardText2;

    [SerializeField]
    private int m_bonusDiamonds;

    private ShopItem[] shopList;
    private int diamondsCount;

    protected void Awake()
    {
        if (Instance != null)
            Destroy(this);
        else
            Instance = this;
        //PlayerPrefs.SetInt("CoinsBank", 4000);

        diamondsCount = PlayerPrefs.GetInt("CoinsBank", 1000);
        PlayerPrefs.SetInt("CoinsBank", diamondsCount);
        m_moneyText.text = diamondsCount.ToString();
        shopList = Resources.Load<ShopItemsData>("Databases/ShopItemsDatabase").ShopDatabase;
        GameObject item = Instantiate(m_ShopDoubleItem);
        item.transform.SetParent(m_Content.transform);
        for (int i = 0; i < shopList.Length; i++)
        {
            item = Instantiate(m_ShopItem);
            item.transform.SetParent(m_Content.transform);
            //int cPrice = shopList[i].Price;
            //string cName = shopList[i].Name;
            //bool cOwned = shopList[i].Owned;
            //shopList[i].Action = delegate { ApplySkin(cPrice, cName, cOwned); };
            var shopItem = item.GetComponent<ShopItemPanel>();
            shopItem.FillItem(shopList[i]);
        }
    }

    void Start()
    {
        IronSourceEvents.onRewardedVideoAdRewardedEvent += RewardPlayer;

        //IronSourcePlacement placement = IronSource.Agent.getPlacementInfo("ExtraDiamonds");
        //if (placement == null)
        //    m_RewardText2.text = "placement is not valid";
        //else
        //    m_RewardText2.text = placement.getPlacementName() + " " + placement.getRewardName() + " " + placement.getRewardAmount();

    }

    private void OnDestroy()
    {
        IronSourceEvents.onRewardedVideoAdRewardedEvent -= RewardPlayer;
    }

    public void UpdateCurrencyBalance(int price)
    {
        diamondsCount += price;
        m_moneyText.text = diamondsCount.ToString();
        PlayerPrefs.SetInt("CoinsBank", diamondsCount);
    }

    public void CloseShop()
    {
        MainMenuController.Instance.CloseShop();
    }

    private void RewardPlayer(IronSourcePlacement obj)
    {
        //m_RewardText.text = obj.getPlacementName();
        //if (obj.getPlacementName() == "ExtraDiamonds")
        UpdateCurrencyBalance(m_bonusDiamonds);
    }

    public void ShowRewardedVideoButtonClicked()
    {
        if (IronSource.Agent.isRewardedVideoAvailable())
        {
            IronSource.Agent.showRewardedVideo("ExtraDiamonds");
        }
    }
}
