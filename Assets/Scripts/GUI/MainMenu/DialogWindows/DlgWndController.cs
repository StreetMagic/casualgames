﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DlgWndController : MonoBehaviour
{
    public static DlgWndController instace;

    public bool isOnPause = false;

    private void Awake()
    {
        if (instace != null)
            Destroy(this);
        else
            instace = this;
        DlgAbstract.DlgWasClosedEvent += DlgWasClosedEvent;
    }

    private void OnDestroy()
    {
        DlgAbstract.DlgWasClosedEvent -= DlgWasClosedEvent;
    }

    private void DlgWasClosedEvent(DlgAbstract obj)
    {
        Destroy(obj.gameObject);
    }
}
