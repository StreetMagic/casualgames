﻿using UnityEngine;
using UnityEngine.UI;

//[RequireComponent(typeof(Animator))]
//[RequireComponent(typeof(Canvas))]
//[RequireComponent(typeof(GraphicRaycaster))]
public abstract class DlgAbstract : MonoBehaviour
{
    public static System.Action<DlgAbstract> DlgWasOpenedEvent;
    public static System.Action<DlgAbstract> DlgWasClosedEvent;
    public Animator Animator { get; protected set; }
    public bool IsOpened { get; private set; }
    protected Canvas m_canvas;

    virtual protected void Awake()
    {
        Animator = GetComponent<Animator>();
        m_canvas = GetComponent<Canvas>();
        if (m_canvas)
            m_canvas.enabled = false;
    }
    virtual public void OpenDlg()
    {
        if (m_canvas)
            m_canvas.enabled = true;
        if (Animator)
            Animator.SetTrigger("Open");
        else DlgHasBeenOpened();
    }
    virtual public void CloseDlg()
    {
        if (Animator)
            Animator.SetTrigger("Close");
        else DisableDlg();
    }

    //Will called from animation
    public void DlgHasBeenOpened()
    {
        if (DlgWasOpenedEvent != null) DlgWasOpenedEvent.Invoke(this);
    }
    //Will called from animation
    void DisableDlg()
    {
        if (DlgWasClosedEvent != null) DlgWasClosedEvent.Invoke(this);
        Destroy(gameObject);
    }

    public virtual void OnCloseBtnDown()
    {
        CloseDlg();
    }
    public virtual void OnOpenBtnDown()
    {
        OpenDlg();
    }
}
