﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class ShopDoubleItemPanel : MonoBehaviour
{
    [SerializeField]
    private Text m_DiamondsCountText;

    [SerializeField]
    private Text m_DiamondsTimerText;

    [SerializeField]
    private GameObject m_DiamondsActive;

    [SerializeField]
    private GameObject m_DiamondsInactive;

    [SerializeField]
    private Text m_BonusesCostText;

    [SerializeField]
    private Text m_BonusesCountText;

    private ShopDoubleItem data;

    private bool isReady;

    private void Start()
    {
        transform.localScale = new Vector3(1f, 1f, 1f);
        data = Resources.Load<ShopItemsData>("Databases/ShopItemsDatabase").DoubleItem;
        SetPanels();
        //TODO isReady check
    }

    public void GetDiamonds()
    {
        if (isReady)
        {
            if (IronSource.Agent.isRewardedVideoAvailable())
            {
                IronSourceEvents.onRewardedVideoAdClosedEvent += AddDiamonds;
                IronSource.Agent.showRewardedVideo();
            }
        }
    }

    private void AddDiamonds()
    {
        m_DiamondsActive.SetActive(false);
        m_DiamondsInactive.SetActive(true);
        isReady = false;
        DlgShop.Instance.UpdateCurrencyBalance(data.DiamondsCount);
        IronSourceEvents.onRewardedVideoAdClosedEvent -= AddDiamonds;
    }

    public void GetBonuses()
    {
        int coinsBank = PlayerPrefs.GetInt("CoinsBank", 0);
        if (coinsBank < data.BonusesCost)
            return;

        int bonusCount = PlayerPrefs.GetInt("ActiveBonusCount", 0);
        bonusCount += data.BonusesCount;
        PlayerPrefs.SetInt("ActiveBonusCount", bonusCount);
        DlgShop.Instance.UpdateCurrencyBalance(-data.BonusesCost);
    }

    private void SetPanels()
    {
        m_DiamondsCountText.text = "+" + data.DiamondsCount.ToString();
        m_BonusesCostText.text = data.BonusesCost.ToString();
        m_BonusesCountText.text = "+" + data.BonusesCount.ToString();
        //TODO cooldown timer
    }
}
