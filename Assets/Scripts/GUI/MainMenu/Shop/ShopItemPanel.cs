﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class ShopItemPanel : MonoBehaviour
{
    [SerializeField]
    private Text m_CostText;

    [SerializeField]
    private Text m_ActiveNameText;

    [SerializeField]
    private Text m_InactiveNameText;

    [SerializeField]
    private Text m_NotOwnedNameText;

    [SerializeField]
    private GameObject m_activeSkin;

    [SerializeField]
    private GameObject m_inactiveSkin;

    [SerializeField]
    private GameObject m_notOwnedSkin;

    [SerializeField]
    private Image m_PromoImage;

    private ShopItemsData data;

    private bool isOwned;
    private int myPrice;
    private string skinName;

    private void Start()
    {
        SkinManager.Instance.OnSkinChanged += UpdateStatus;
        transform.localScale = new Vector3(1f, 1f, 1f);
        data = Resources.Load<ShopItemsData>("Databases/ShopItemsDatabase");
    }

    private void UpdateStatus()
    {
        string activeSkinName = PlayerPrefs.GetString("ActiveSkinName");
        if (isOwned)
        {
            if (skinName != activeSkinName)
            {
                m_activeSkin.SetActive(false);
                m_inactiveSkin.SetActive(true);
                m_notOwnedSkin.SetActive(false);
            }
            else
            {
                m_activeSkin.SetActive(true);
                m_inactiveSkin.SetActive(false);
                m_notOwnedSkin.SetActive(false);
            }
        }
        else
        {
            m_activeSkin.SetActive(false);
            m_inactiveSkin.SetActive(false);
            m_notOwnedSkin.SetActive(true);
        }
    }

    public void SetPrice(int price)
    {
        myPrice = price;
        m_CostText.text = price.ToString() + " COINS";
    }

    //public void SetName(string name)
    //{
    //    skinName = name;
    //    List<char> separetedName = new List<char>();
    //    for (int j = 0; j < name.Length; j++)
    //    {
    //        if (Char.IsUpper(name[j]) && j != 0)
    //        {
    //            separetedName.Add(' ');
    //        }
    //        separetedName.Add(name[j]);
    //    }
    //    string newName = null;
    //    foreach (var c in separetedName)
    //    {
    //        newName += c;
    //    }
    //    newName = newName.ToUpper();
    //    m_HeaderHalf.text = newName;
    //}

    public void FillItem(ShopItem item)
    {
        skinName = item.Name;
        m_ActiveNameText.text = m_InactiveNameText.text = m_NotOwnedNameText.text = skinName.ToString();
        SetPrice(item.Price);
        isOwned = item.Owned;
        m_PromoImage.sprite = item.PromoIcon;
        UpdateStatus();
    }

    private void OnDestroy()
    {
        SkinManager.Instance.OnSkinChanged -= UpdateStatus;
    }

    public void BuyOrApplySkin()
    {
        if (!isOwned)
        {
            int coinsBank = PlayerPrefs.GetInt("CoinsBank", 0);
            if (coinsBank < myPrice)
                return;

            isOwned = true;
            data.SetObjectByName(skinName);
            UpdateStatus();
            DlgShop.Instance.UpdateCurrencyBalance(-myPrice);
            SaveData();
            return;
        }
        else
        {
            string activeSkinName = PlayerPrefs.GetString("ActiveSkinName");
            if (skinName == activeSkinName)
                return;

            SkinManager.Instance.SetSkinName(skinName);
        }
    }

    [Header("Meta")]
    [SerializeField]
    private string persisterName;
    [Header("Scriptable Objects")]
    [SerializeField]
    private ScriptableObject[] objectsToPersist;

    private void LoadData()
    {
        for (int i = 0; i < objectsToPersist.Length; i++)
        {
            if (File.Exists(Application.persistentDataPath + string.Format("/{0}_{1}.pso", persisterName, i)))
            {
                BinaryFormatter bf = new BinaryFormatter();
                FileStream file = File.Open(Application.persistentDataPath + string.Format("/{0}_{1}.pso", persisterName, i), FileMode.Open);
                JsonUtility.FromJsonOverwrite((string)bf.Deserialize(file), objectsToPersist[i]);
                file.Close();

            }
            else
            {
                //Do Nothing
            }
        }
    }

    protected void SaveData()
    {
        for (int i = 0; i < objectsToPersist.Length; i++)
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Create(Application.persistentDataPath + string.Format("/{0}_{1}.pso", persisterName, i));
            var json = JsonUtility.ToJson(objectsToPersist[i]);
            bf.Serialize(file, json);
            file.Close();
        }

    }
}
