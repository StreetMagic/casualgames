﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxSpawner : MonoBehaviour
{
    public static BoxSpawner Instance { get; private set; }

    [SerializeField]
    private GameObject m_Box;

    public float SpawnDelay;

    void Awake()
    {
        if (Instance != null)
            Destroy(this);
        else
            Instance = this;
    }

    // Use this for initialization
    void Start()
    {
        StartCoroutine(SpawnCoroutine());
    }

    private IEnumerator SpawnCoroutine()
    {
        while (!Game3Controller.Instance.GameIsOver)
        {
            Vector3 pos = transform.position;
            pos.x += Random.Range(-20, 20);
            var box = Instantiate(m_Box, pos, m_Box.transform.rotation);
            box.GetComponent<FallingBox>().m_Speed = Game3Controller.Instance.BoxSpeed;
            yield return new WaitForSeconds(SpawnDelay);
        }
    }
}
