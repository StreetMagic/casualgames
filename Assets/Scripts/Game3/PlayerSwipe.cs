﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSwipe : MonoBehaviour
{
    [SerializeField]
    private int m_Force;

    private Camera mainCam;

    void Start()
    {
        mainCam = Camera.main;
        InputManager.Instance.SwipeAction += SwipeBox;
    }

    private void OnDestroy()
    {
        InputManager.Instance.SwipeAction -= SwipeBox;
    }

    private void SwipeBox(InputData data)
    {
        if (Game3Controller.Instance.GameIsOver)
            return;

        Ray ray = mainCam.ScreenPointToRay(data.StartPositon);
        RaycastHit2D hit = Physics2D.Raycast(ray.origin, ray.direction);
        if (hit)
        {
            var box = hit.collider.GetComponent<FallingBox>();
            if (box && !box.GetComponent<Floor>() && !box.IsSwiped)
            {
                box.IsSwiped = true;
                box.GetComponent<Rigidbody2D>().velocity = (data.EndPosition - data.StartPositon).normalized * m_Force;
                //switch (data.swipeDirection)
                //{
                //    case SwipeDirections.down:
                //        box.IsSwiped = true;
                //        box.GetComponent<Rigidbody2D>().velocity = Vector2.down * m_Force;
                //        break;
                //    case SwipeDirections.right:
                //        box.IsSwiped = true;
                //        box.GetComponent<Rigidbody2D>().velocity = Vector2.right * m_Force;
                //        break;
                //    case SwipeDirections.left:
                //        box.IsSwiped = true;
                //        box.GetComponent<Rigidbody2D>().velocity = Vector2.left * m_Force;
                //        break;
                //    default:
                //        break;
                //}
            }
        }
    }
}