﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Game3Controller : MonoBehaviour
{
    public static Game3Controller Instance { get; private set; }

    [SerializeField]
    private Text m_scoreText;

    [SerializeField]
    private float boxSpeed;
    public float BoxSpeed
    {
        get { return boxSpeed; }
        private set { boxSpeed = value; }
    }

    public Action OnLoseAction;
    private int score;
    private int realScore;
    private bool gameIsOver = false;
    public bool GameIsOver
    {
        get { return gameIsOver; }
        set
        {
            gameIsOver = value;
            if (gameIsOver == true)
                m_scoreText.text = "GAME OVER!";
        }
    }


    void Awake()
    {
        if (Instance != null)
            Destroy(this);
        else
            Instance = this;
    }

    public void UpdateScore(int points)
    {
        score += points;
        if (realScore < score && points > 0)
        {
            realScore++;
            if (realScore % 10 == 0)
            {
                BoxSpeed += 5f;
                BoxSpeed = Mathf.Clamp(BoxSpeed, 0, 200);
            }

            if (realScore % 20 == 0)
            {
                BoxSpawner.Instance.SpawnDelay -= 0.5f;
                BoxSpawner.Instance.SpawnDelay = Mathf.Clamp(BoxSpawner.Instance.SpawnDelay, 0.1f, 5f);
            }
        }


        score = Mathf.Clamp(score, 0, 9999999);
        m_scoreText.text = score.ToString();
    }
}
