﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallingBox : MonoBehaviour
{
    [SerializeField]
    private Material[] m_Materials;

    [SerializeField]
    public float m_Speed;

    public PartColors color;

    private bool isSwiped = false;
    public bool IsSwiped
    {
        get { return isSwiped; }
        set
        {
            if (value)
                StopAllCoroutines();
            isSwiped = value;
        }
    }


    private void Start()
    {
        int nm = Random.Range(1, 3);
        color = (PartColors)nm;
        GetComponent<SpriteRenderer>().material = m_Materials[nm];
        StartCoroutine(MoveCoroutine());
    }

    private IEnumerator MoveCoroutine()
    {
        while (true)
        {
            transform.Translate(Vector3.down * Time.deltaTime * m_Speed);
            yield return null;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.GetComponent<Floor>() && !IsSwiped)
        {
            StopAllCoroutines();
            GetComponent<SpriteRenderer>().material = m_Materials[4];
            gameObject.AddComponent<Floor>();
            GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Dynamic;

            if (transform.position.y >= 40f)
            {
                Game3Controller.Instance.GameIsOver = true;
            }
        }
    }
}