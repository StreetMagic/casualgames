﻿using UnityEngine;
using UnityEngine.UI;

public class Floor : MonoBehaviour
{
    [SerializeField]
    private PartColors m_CollectorColor;

    private int m_score;

    private void OnCollisionEnter2D(Collision2D collision)
    {
        var obj = collision.gameObject.GetComponent<FallingBox>();
        if (obj && obj.IsSwiped)
        {
            if (obj.color == m_CollectorColor)
            {
                Game3Controller.Instance.UpdateScore(1);
                Destroy(obj.gameObject, 0.5f);
            }
            else
            {
                Game3Controller.Instance.UpdateScore(-1);
                Destroy(obj.gameObject, 0.5f);
            }
        }
    }
}
