﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowVideoInMenu : MonoBehaviour
{
    [SerializeField]
    private int m_bonusDiamonds;
    // Use this for initialization
    void Start()
    {
        IronSourceEvents.onRewardedVideoAdRewardedEvent += RewardPlayer;
    }

    private void OnDestroy()
    {
        IronSourceEvents.onRewardedVideoAdRewardedEvent -= RewardPlayer;
    }

    private void RewardPlayer(IronSourcePlacement obj)
    {
        DlgShop.Instance.UpdateCurrencyBalance(m_bonusDiamonds);
    }

    public void ShowRewardedVideoButtonClicked()
    {
        if (IronSource.Agent.isRewardedVideoAvailable())
        {
            IronSource.Agent.showRewardedVideo();
        }
    }
}
