using UnityEngine;
using System;
using System.Collections;

public class ShowRewardedVideoScript : MonoBehaviour
{
    [SerializeField]
    private GameObject m_timeOutPanel;

    [SerializeField]
    private GameOverPanel m_gameOverPanel;

    // Use this for initialization
    void Start()
    {
        IronSourceEvents.onRewardedVideoAdRewardedEvent += RewardPlayer;
        IronSourceEvents.onRewardedVideoAdClosedEvent += AddTime;
    }


    private void OnDestroy()
    {
        //Add Rewarded Video Events
        IronSourceEvents.onRewardedVideoAdRewardedEvent -= RewardPlayer;
        IronSourceEvents.onRewardedVideoAdClosedEvent -= AddTime;
    }

    private void RewardPlayer(IronSourcePlacement reward)
    {
        if (reward.getPlacementName() == "ExtraDiamonds")
        {
            CurrencyManager.Instance.AddCoins(CurrencyManager.Instance.BonusDiamonds);
            m_gameOverPanel.UpdateDiamonds();
        }
    }

    public void AddTime()
    {
        TimeManager.Instance.Timer += 30f;
        TimeManager.Instance.timeIsOut = false;
        m_timeOutPanel.SetActive(false);
        FieldController.Instance.canPush = true;
    }


    /************* RewardedVideo API *************/
    public void ShowRewardedVideoButtonClicked()
    {
        Debug.Log("unity-script: ShowRewardedVideoButtonClicked");
        if (IronSource.Agent.isRewardedVideoAvailable())
        {
            IronSource.Agent.showRewardedVideo();
        }
        else
        {
            m_timeOutPanel.SetActive(false);
            GameMenuUIManager.Instance.GameOver();
        }
    }

    [ContextMenu("add diamonds")]
    public void ShowDiamonds()
    {
        int bonusCoins = (int)(CurrencyManager.Instance.GetRoundCurrency() * 0.2f);
        CurrencyManager.Instance.AddCoins(bonusCoins);
        print(bonusCoins + " and Total: " + CurrencyManager.Instance.GetRoundCurrency());
    }
}
