﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Game7Controller : MonoBehaviour
{
    public static Game7Controller Instance { get; private set; }

    [SerializeField]
    private Transform[] m_roadLines;

    [SerializeField]
    private GameObject m_car;

    [SerializeField]
    private Text m_scoreText;

    [SerializeField]
    private float m_carSpeed;

    [SerializeField]
    private float m_carSpawnTime;

    [SerializeField]
    private int m_carSpawnLimit;

    private float timer; 

    private bool gameIsOver = false;
    public bool GameIsOver
    {
        get { return gameIsOver; }
        set
        {
            gameIsOver = value;
            if (gameIsOver == true)
                m_scoreText.text = "GAME OVER!";
        }
    }

    private int score;

    private void Awake()
    {
        if (Instance != null)
            Destroy(this);
        else
            Instance = this;
    }

    void Start()
    {
        StartCoroutine(CarGenerator());
    }


    private void Update()
    {
        timer += Time.deltaTime;
        if(timer > 30)
        {
            if (m_carSpawnLimit >= 4)
                return;
            m_carSpawnLimit++;
            timer = 0;
        }
    }

    public void RemovePoints(int points)
    {
        score -= points;
        m_scoreText.text = score.ToString();
    }

    public void AddPoints(int points)
    {
        score += points;
        m_scoreText.text = score.ToString();
    }

    private void RoadCheck(List<int> roads)
    {
        int p = Random.Range(1, 5);
        if (roads.Contains(p))
        {
            Vector3 pos = m_roadLines[p].position;
            pos.y = 60f;
            var car = Instantiate(m_car, pos, m_car.transform.rotation);
            car.GetComponent<CarBehavior>().SetCar(m_carSpeed, m_roadLines, p);
            roads.Remove(p);
        }
        else RoadCheck(roads);
    }

    private IEnumerator CarGenerator()
    {
        while (!GameIsOver)
        {
            int n = Random.Range(1, m_carSpawnLimit);
            List<int> roads = new List<int> { 1, 2, 3, 4, 5 };
            for (int i = 0; i < n; i++)
            {
                RoadCheck(roads);
            }
            yield return new WaitForSeconds(m_carSpawnTime);
        }
    }
}