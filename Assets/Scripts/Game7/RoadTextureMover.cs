﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoadTextureMover : MonoBehaviour
{
    [SerializeField]
    private Material m_material;

    [SerializeField]
    private float m_speed;

    private Vector2 offset = Vector2.zero;

    private void Start()
    {
        m_material.mainTextureOffset = offset;
    }

    void Update()
    {
        offset.x += m_speed;
        m_material.mainTextureOffset = offset;
    }
}
