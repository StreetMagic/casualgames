﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCarBehavior : MonoBehaviour
{
    [SerializeField]
    private Transform[] m_roadLines;

    [SerializeField]
    private GameObject m_rightArrow;

    [SerializeField]
    private GameObject m_leftArrow;

    [SerializeField]
    private float m_speed;

    [SerializeField]
    private float m_changeLineTime;

    [SerializeField]
    private float m_ChangeLineDelay;

    private IEnumerator myCoroutine;

    private float timer;
    private int currentRoadLine = 0;
    private bool isMoving = false;

    void Start()
    {
        timer = m_changeLineTime;
    }

    void Update()
    {
        if (Game7Controller.Instance.GameIsOver)
            return;

        if (timer <= 0 && !isMoving)
        {
            int nm = currentRoadLine;
            while (nm != currentRoadLine + 1 && nm != currentRoadLine - 1)
            {
                nm = Random.Range(0, 3);
            }

            if (nm > currentRoadLine)
                m_rightArrow.SetActive(true);
            else
                m_leftArrow.SetActive(true);

            currentRoadLine = nm;
            SwitchLine(currentRoadLine);
            isMoving = true;
        }
        else
            timer -= Time.deltaTime;
    }

    private void SwitchLine(int lineNumber)
    {
        if (myCoroutine != null)
            return;

        myCoroutine = CarMoveCoroutine(lineNumber);
        StartCoroutine(myCoroutine);
    }

    private IEnumerator CarMoveCoroutine(int nm)
    {
        yield return new WaitForSeconds(m_ChangeLineDelay);
        m_rightArrow.SetActive(false);
        m_leftArrow.SetActive(false);

        Vector3 direction = m_roadLines[nm].position - transform.position;
        float sqrDist = (transform.position - m_roadLines[nm].position).sqrMagnitude;
        while (sqrDist > 0.08f)
        {
            transform.Translate(direction.normalized * m_speed * Time.deltaTime);
            sqrDist = (transform.position - m_roadLines[nm].position).sqrMagnitude;
            yield return null;
        }
        transform.position = m_roadLines[nm].position;
        timer = m_changeLineTime;
        isMoving = false;
        myCoroutine = null;
    }
}
