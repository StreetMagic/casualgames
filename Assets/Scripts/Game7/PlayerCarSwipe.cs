﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCarSwipe : MonoBehaviour
{
    private Camera mainCam;

    void Start()
    {
        mainCam = Camera.main;
        InputManager.Instance.SwipeAction += SwipeCar;
    }


    private void OnDestroy()
    {
        InputManager.Instance.SwipeAction -= SwipeCar;
    }

    private void SwipeCar(InputData data)
    {
        if (Game7Controller.Instance.GameIsOver)
            return;

        Ray ray = mainCam.ScreenPointToRay(data.StartPositon);
        RaycastHit2D hit = Physics2D.Raycast(ray.origin, ray.direction);
        if (hit)
        {
            var car = hit.collider.GetComponent<CarBehavior>();
            if (car)
            {
                if (data.swipeDirection == SwipeDirections.right)
                    car.MoveCar(true);
                else if(data.swipeDirection == SwipeDirections.left)
                    car.MoveCar(false);
            }
        }
    }
}
