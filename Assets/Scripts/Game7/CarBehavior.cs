﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarBehavior : MonoBehaviour
{
    [SerializeField]
    private float changeLineSpeed;

    private float mySpeed;
    public Transform[] roadLines;
    private IEnumerator myCoroutine;

    private int currentRoadLine;
    public bool isMoving = false;

    [SerializeField]
    private LayerMask m_mask;

    private bool addPoint = true;

    // Update is called once per frame
    void Update()
    {
        transform.Translate(Vector3.down * mySpeed * Time.deltaTime);
        if (transform.position.y < -60 || Game7Controller.Instance.GameIsOver)
            Destroy(gameObject);
    }

    public void SetCar(float speed, Transform[] roads, int startRoad)
    {
        roadLines = roads;
        mySpeed = speed;
        currentRoadLine = startRoad;
    }

    public void MoveCar(bool toTheRight)
    {
        if (myCoroutine != null)
            return;

        if (addPoint)
        {
            if (Physics2D.Raycast(transform.position, Vector2.down, 300f, m_mask))
            {
                Game7Controller.Instance.AddPoints(1);
                addPoint = false;
            }
        }

        if (toTheRight)
        {
            currentRoadLine++;
            StartCoroutine(ChangeLineCoroutine(true));
        }
        else
        {
            currentRoadLine--;
            StartCoroutine(ChangeLineCoroutine(false));
        }
    }

    private IEnumerator ChangeLineCoroutine(bool toTheRight)
    {
        isMoving = true;
        Vector3 direction = toTheRight ? Vector3.right : Vector3.left;
        float dist = Mathf.Abs(transform.position.x - roadLines[currentRoadLine].position.x);
        while (dist > 0.5f)
        {
            transform.Translate(direction * changeLineSpeed * Time.deltaTime);
            dist = Mathf.Abs(transform.position.x - roadLines[currentRoadLine].position.x);
            yield return null;
        }
        Vector3 pos = transform.position;
        pos.x = roadLines[currentRoadLine].position.x;
        transform.position = pos;
        isMoving = false;
        myCoroutine = null;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.GetComponent<CarBehavior>())
        {
            if (isMoving)
            {
                Game7Controller.Instance.RemovePoints(1);
                Destroy(gameObject);
            }
        }
        else if (collision.gameObject.GetComponent<RoadWall>())
        {
            Game7Controller.Instance.RemovePoints(3);
            Destroy(gameObject);
        }
        else if (collision.gameObject.GetComponent<PlayerCarBehavior>())
        {
            Game7Controller.Instance.GameIsOver = true;
        }
    }
}